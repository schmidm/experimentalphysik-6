\minisec{Ströme im p-n-Übergang}

Wie bereits erklärt fließen die Ladungsträger jeweils in Gebiete
niedrigerer Konzentration.  Folglich fließen Elektron in Richtung
p-Gebiet, Löcher in Richtung n-Gebiet.  Am Kontakt treffen sich
Elektronen und Löcher und rekombinieren.  Das Fließen der Elektronen
und Löcher zum Kontakt zum Ausgleich der Konzentration wird als
\acct{Diffusionsstrom} (Rekombinationsstrom) bezeichnet.
% Die Elektronen (Löcher) die im Leitungsband (Valenzband) eine sehr
% hohe Energie haben (Boltzmann-Ausläufer) und die nach dem
% Durchlaufen der Raumladungszone mit den entgegengesetzt geladenen
% Ladungsträgern rekombinieren.
\begin{equation}
  \label{eq:9.42}
  j^d = j_n^d + j_p^d = e \left(D_n \frac{\partial n}{\partial x} - D_p \frac{\partial p}{\partial x}\right)  \; .
\end{equation}
Das Fehlen von freien Ladungsträgern in der Verarmungszone bewirkt,
dass die Ladung der Dotieratome nicht mehr kompensiert werden kann.
Da diese ortsfest sind und nicht diffundieren können erzeugen sie eine
elektrisches Feld, welches auf die freien Ladungsträger wirkt.  Das
Feld ist so gerichtet, dass der daraus resultierende Strom der
Ladungsträger dem Diffusionsstrom entgegen gerichtet ist.  Diesen
Strom nennt man \acct{Feldstrom} (Generationsstrom).  Im
thermodynamischen Gleichgewicht werden ständig Elektron-Loch-Paare
gebildet, wobei die Elektronen und Löcher sofort über die p-n-Schicht
fließen.
\begin{equation}
  \label{eq:9.43}
  j^f = j_n^f + j_p^f = e ( n\mu_n + p\mu_p ) E_x \; .
\end{equation}
Im thermodynamischen Gleichgewicht kompensieren sich die Ströme
\begin{equation}
  \label{eq:9.44}
  j^d + j^f = 0 \; .
\end{equation}
Dies gilt auch für die Elektronen- und Löcher-Ströme einzeln.  Mit
\eqref{eq:9.42} und \eqref{eq:9.43} und
\begin{equation*}
  E_x = - \frac{\partial \tilde V(x)}{\partial x}
\end{equation*}
gilt demnach für die Elektronen
\begin{equation}
  \label{eq:9.45}
  D_n \frac{\partial n}{\partial x} = n \mu_n \frac{\partial \tilde V(x)}{\partial x} \; .
\end{equation}
Setzen wir nun noch für $n(x)$ die Boltzmann-Verteilung aus
\eqref{eq:9.31a} ein
\begin{equation}
  \label{eq:9.46}
  n(x) = N_{\text{eff}}^L \exp\left[ - \left(\frac{E_L - e \tilde V(x) - E_F}{\kB T} \right) \right]
\end{equation}
deren Ableitung nach $x$ durch
\begin{equation}
  \label{eq:9.47}
  \frac{\partial n}{\partial x} = n \frac{e}{\kB T} \frac{\partial \tilde V(x)}{\partial x}
\end{equation}
gegeben ist, so finden wir, dass die Transportgrößen über die
Einstein-Beziehung verknüpft sind.
\begin{equation}
  \label{eq:9.48}
  \begin{aligned}
    D_n &= \frac{\kB T}{e} \mu_n \\
    D_p &= \frac{\kB T}{e} \mu_p
  \end{aligned}
\end{equation}

Ist die Raumladungszone dünn und die Rekombinationsrate klein, so
können wir annehmen, dass der Rekombinationsstrom von Elektronen und
Löchern in der Raumladungszone vernachlässigt werden kann und die
Diffusionslänge ist viel größer als die Dicke der Raumladungszone
\begin{equation*}
  L_D \gg d_n + d_p \; .
\end{equation*}
Elektronen und Löcher können also bis ins umgekehrt dotierte Gebiet
laufen.  Dann hängt die Stärke des Stroms nicht mehr vom
Potentialverlauf ab, da außerhalb der Raumladungszone das Potential
konstant ist.  Allerdings bleibt dennoch eine Potentialdifferenz
zwischen den beiden Gebieten, gegen die die Ladungsträger anlaufen
müssen.  Die Höhe der Barriere ist $e V_D$.  Damit die Ladungsträger
über die Barriere springen können müssen sie genügend thermische
Energie haben.  Der Strom der Ladungsträger, die die Barriere
überwinden können ist also proportional zu einem Boltzmann-Faktor.
\begin{equation*}
  j^d \sim \ee^{-(e V_D)/\kB T}
\end{equation*}
Mit \eqref{eq:9.44} finden wir
\begin{equation}
  \label{eq:9.49}
  |j^d| = |j^f| = a(T) \ee^{-(e V_D)/\kB T}
\end{equation}
mit dem Vorfaktor $a(T)$, der schwach von der Temperatur abhängt.

\minisec{Der p-n-Übergang unter äußerer Spannung}

Eine äußere Spannung stört das Gleichgewicht von Feld- und
Diffusionsstrom. Die Gleichgewichtsthermodynamik ist nicht mehr
anwendbar. Eine angelegte Spannung $U$ fällt hauptsächlich in der
Raumladungszone ab, da dort wenig Ladungsträger vorhanden sind und
damit der Widerstand groß ist.  Der restliche Halbleiter ist nahezu
feldfrei und wir schreiben deshalb
\begin{equation}
  \label{eq:9.50}
  \tilde V_{\text{n}}(\infty) - \tilde V_{\text{p}}(-\infty) = V_D - U \; .
\end{equation}

\begin{theorem}[Konvention]
  Eine positive Spannung ist der Diffusionsspannung
  entgegengerichtet. Eine positive Polung bei p und eine negative
  Polung bei n entspricht der Durchlassrichtung. Der umgekehrte Fall
  ist die Sperrrichtung.
\end{theorem}

In der Raumladungszone sind die Ladungsträger nicht im Gleichgewicht,
d.h.\ sie haben kein gemeinsames Fermi-Niveau. Falls aber die
Elektronen, bzw.\ Löcher untereinander im Gleichgewicht sind gibt es
ein Quasi-Fermi-Niveau der Elektronen $E_F^{\text{n}}$ und der Löcher
$E_F^{\text{p}}$.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,xscale=.8,yscale=1]
    \begin{scope}
      \fill[MidnightBlue!20] (0,1.5) -- (2,1.5) to[out=0,in=180] (4,.7) -- (6,.7) |- (0,2) -- cycle;
      \fill[MidnightBlue!40] (0,0) -- (2,0) to[out=0,in=180] (4,-.5) -- (6,-.5) |- (0,-2.5) -- cycle;
      \draw (0,-2.5) rectangle (6,2);
      \draw (0,1.5) -- (2,1.5) to[out=0,in=180] (4,.7) -- (6,.7) node[right] {$E_L$};
      \draw (0,0) -- (2,0) to[out=0,in=180] (4,-.5) -- (6,-.5) node[right] {$E_V$};
      \draw[DarkOrange3,dotted] (0,.1) -- (1.5,.1) to[out=0,in=180] (2.5,.6) -- (6,.6) node[below right] {$E_F^{\text{n}}$};
      \draw[MidnightBlue,dotted] (0,.1) node[left] {$E_F^{\text{p}}$} -- (3.5,.1) to[out=0,in=180] (4.5,.6) -- (6,.6);
      \draw[dashed] (2,1.5) -- (6,1.5);
      \draw[<->] (4.5,1.5) -- node[fill=MidnightBlue!20,inner sep=0pt] {$-e(V_D-|U|)$} (4.5,.7);
      \draw (3,-2.7) -- (3,2.3) node[left] {\strut p-dotiert} node[right] {\strut n-dotiert};
    \end{scope}
    \begin{scope}[shift={(8,0)}]
      \fill[MidnightBlue!20] (0,1.5) -- (2,1.5) to[out=0,in=180] (4,-.5) -- (6,-.5) |- (0,2) -- cycle;
      \fill[MidnightBlue!40] (0,0) -- (2,0) to[out=0,in=180] (4,-2) -- (6,-2) |- (0,-2.5) -- cycle;
      \draw (0,-2.5) rectangle (6,2);
      \draw (0,1.5) -- (2,1.5) to[out=0,in=180] (4,-.5) -- (6,-.5) node[right] {$E_L$};
      \draw (0,0) -- (2,0) to[out=0,in=180] (4,-2) -- (6,-2) node[right] {$E_V$};
      \draw[DarkOrange3,dotted] (0,.1) -- (1.5,.1) to[out=0,in=180] (2.5,-.6) -- (6,-.6) node[below right] {$E_F^{\text{n}}$};
      \draw[MidnightBlue,dotted] (0,.1) node[left] {$E_F^{\text{p}}$} -- (3.5,.1) to[out=0,in=180] (4.5,-.6) -- (6,-.6);
      \draw[dashed] (2,1.5) -- (6,1.5);
      \draw[<->] (4.5,1.5) -- node[fill=MidnightBlue!20,inner sep=0pt] {$-e(V_D+|U|)$} (4.5,-.5);
      \draw (3,-2.7) -- (3,2.3) node[left] {\strut p-dotiert} node[right] {\strut n-dotiert};
    \end{scope}
  \end{tikzpicture}
  \caption{p-n-Kontakt unter äußerer Spannung.  Es stellen sich
    Quasi-Fermi-Niveaus für Elektronen und Löcher ein (gepunktet).  In
    Durchlassrichtung verringert eine angelegte Spannung die
    Potentialdifferenz (links), in Sperrrichtung wird die
    Potentialdifferenz erhöht (rechts).}
  \label{fig:8.1}
\end{figure}

Wir interessieren uns nun für den Einfluss der angelegten Spannung auf
den Diffusions- und den Feldstrom.  Der Feldstrom wird in erster
Näherung nicht beeinflusst. Jeder Ladungsträger innerhalb der
Raumladungszone wird abgesaugt und durchquert die Raumladungszone.
Für die Elektronen gilt also
\begin{equation*}
  j_n^f(U) = j_n^f(0) \; .
\end{equation*}
Der Diffusionsstrom ändert sich, da die Potentialbarriere geändert
wird von $V_D$ auf $V_D - U$.
\begin{equation*}
  j_n^d(U) = a(T) \ee^{-e(V_D-U)/\kB T} = j_n^d(0) \ee^{eU/\kB T} \; .
\end{equation*}
Feld- und Diffusionsstrom fließen entgegengesetzt, also gilt für den
gesamten Strom der Elektronen
\begin{equation*}
  j_n(U) = j_n^d(U) - j_n^f = j_n^f \ee^{eU/\kB T} - j_n^f = j_n^f \left( \ee^{eU/\kB T} - 1 \right) \; .
\end{equation*}
wobei $|j_n^d(0)| = |j_n^f(0)|$.

Zählen wir nun noch den Strom von Elektronen und Löchern zusammen, da
beide zum Ladungstransport beitragen, so gilt für die gesamte
Stromdichte
\begin{equation}
  \label{eq:9.51}
  j(U) = (j_n^f + j_p^f) \left( \ee^{eU/\kB T} - 1 \right) = j_S \left( \ee^{eU/\kB T} - 1 \right) \; .
\end{equation}
In Abbildung~\ref{fig:8.2} ist der Strom über die Spannung
aufgetragen.  Dieses Diagramm nennt man auch
\acct{Strom-Spannungs-Kennlinie}.  Legt man eine Spannung in
Durchlassrichtung an, so wächst der Strom gemäß \eqref{eq:9.51}
exponentiell an.  In Sperrrichtung fließt höchstens der Feldstrom
$j_S$.  Erhöht man die Spannung in Sperrrichtung jedoch sehr stark so
setzt der sogennannte \acct{Zener-Durchbruch} ein.  Dabei tunneln
Elektronen vom Valenzband des p-dotierten Gebietes in das Leitungsband
des n-dotierten Gebietes, da die Leitungsbandkante unter die
Valenzbandkante verschoben wird, vgl.\ Abbildung~\ref{fig:8.1} rechts.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (-2,0) -- (2,0) node[below] {$U$};
    \draw[->] (0,-1) -- (0,2) node[left] {$I$};
    \draw[MidnightBlue] (-2,-.2) -- (-.7,-.2) to[out=0,in=-100] (1,2);
    \draw[MidnightBlue,dashed] (-2,-1) node (A) {} to[out=80,in=180] (-1,-.2);
    \node[MidnightBlue] at (-1,.5) {$-(j_n^f + j_p^f)$};
    \node[pin={below right:Zenerdurchbruch}] at (A) {};
  \end{tikzpicture}
  \caption{Strom-Spannungs-Kennlinie einer $p$-$n$-Diode.  Idealisiert
    bleibt der Strom in Sperrrichtung konstant.  In der Realität kommt
    es jedoch zum Zenerdurchbruch.}
  \label{fig:8.2}
\end{figure}

Die Berechnung von $(j_n^f + j_p^f)$ ist etwas aufwändiger.  Wir
verweisen daher nur auf \textcite[435--439]{ibach-lueth}.  Dort findet
man
\begin{align}
  \label{eq:9.52}
  j(U) &= \left( \frac{e D_p}{L_p} p_n + \frac{e D_n}{L_n} n_p \right) \left( \ee^{eU/\kB T} - 1 \right)
\end{align}
mit den Diffusionslängen der Elektronen und Löcher
\begin{align*}
  L_n = \sqrt{D_n \tau_n} \quad L_p = \sqrt{D_p \tau_p} 
\end{align*}
mit der mittleren Lebensdauer der Elektronen $\tau_n$, bzw.\ der
Löcher $\tau_p$.

\subsection{Metall-Halbleiter-Kontakt}

% \begin{center}
%   \begin{tikzpicture}[gfx]
%     \begin{scope}
%       \draw (0,2) -- (1,2) |- (3,0) node[right] {$E_F$};
%       \draw[dashed] (1,2) -- (3,2) node[right] {$E_{\text{Vak}}$};
%       \draw[DarkOrange3,<->] (2,2) -- node[right] {$\phi_M$ Austrittsarbeit} (2,0);
%     \end{scope}
%     \begin{scope}[shift={(5,0)}]
%       \draw (0,2) -- (1,2) |- (3,0) node[right] {$E_V$};
%       \draw[dashed] (1,2) -- (3,2) node[right] {$E_{\text{Vak}}$};
%       \draw (1,1.5) node[left] {$E_L$} -- (3,1.5);
%       \draw[dashed] (1,1) -- (3,1) node[right] {$E_F$};
%       \draw[DarkOrange3,<->] (1.5,2) node[above] {$\phi_M$} -- (1.5,1);
%       \draw[MidnightBlue,<->] (2.5,2) node[above] {$\chi$} -- (2.5,1.5);
%     \end{scope}
%   \end{tikzpicture}
% \end{center}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,phi/.style={fill=white,inner sep=1pt}]
    \begin{scope}
      \fill[MidnightBlue!40] (0,0) rectangle (1,2.5);
      \fill[MidnightBlue!20] (1.5,0) rectangle (4,1);
      \draw (0,0) rectangle (4,4);
      \draw (0,3.5) -- (4,3.5) node[right] {$E_{\text{Vak}}$};
      % Metall
      \draw (1,3.5) -- (1,0);
      \draw (1,2.5) -- (0,2.5) node[left] {$E_F$};
      \node[above] at (.5,0) {ME};
      \draw[<->] (.5,3.5) -- node[phi] {$\phi_{\text{ME}}$} (.5,2.5);
      % n-Halbleiter
      \draw (1.5,3.5) -- (1.5,0);
      \draw (1.5,1) -- (4,1);
      \node[above] at (2.75,0) {n-HL};
      \draw (1.5,2) -- (4,2) coordinate[pin={above right:$E_L$}];
      \draw[DarkOrange3] (1.5,1.8) -- (4,1.8) node[right] {$E_D$};
      \draw (1.5,1.6) -- (4,1.6) coordinate[pin={below right:$E_F$}];
      \draw[<->] (2.75,3.5) -- node[phi] {$\phi_{\text{HL}}$} (2.75,1.6);
      \draw[Purple,<->] (3.2,3.5) -- node[phi] {$\chi$} (3.2,2);
    \end{scope}

    \begin{scope}[shift={(6,0)}]
      \fill[MidnightBlue!40] (0,0) rectangle (1,1.6);
      \fill[MidnightBlue!20] (1,.1) to[out=60,in=180] (2,1) -- (4,1) |- (1,0) -- cycle;
      \draw (0,0) rectangle (4,4);
      \draw (0,2.6) -- (1,2.6) to[out=60,in=180] (2,3.5) -- (4,3.5) node[right] {$E_{\text{Vak}}$};
      \draw (4,1.6) -- (0,1.6) node[left] {$E_F$};
      % Metall
      \draw (1,1.6) -- (1,0);
      \node[above] at (.5,0) {ME};
      \draw[<->] (.5,2.6) -- node[phi] {$\phi_{\text{ME}}$} (.5,1.6);
      % n-Halbleiter
      \draw (1,.1) to[out=60,in=180] (2,1) -- (4,1);
      \node[above] at (2.75,0) {n-HL};
      \draw (1,1.1) to[out=60,in=180] (2,2) -- (4,2) coordinate[pin={above right:$E_L$}];
      \draw[DarkOrange3] (1,.9) to[out=60,in=180] (2,1.8) -- (4,1.8) node[right] {$E_D$};
      \draw[<->] (2.75,3.5) -- node[phi] {$\phi_{\text{HL}}$} (2.75,1.6);
      \draw[Purple,<->] (3.2,3.5) -- node[phi] {$\chi$} (3.2,2);
      % Entartetes Fermi-Gas
      \fill (1,1.1) -- (1.3,1.6) -- (1,1.6) -- cycle;
    \end{scope}

    \begin{scope}[shift={(0,-5)}]
      \fill[MidnightBlue!40] (0,0) rectangle (1,1.3);
      \fill[MidnightBlue!20] (1.5,0) rectangle (4,1);
      \draw (0,0) rectangle (4,4);
      \draw (0,3.5) -- (4,3.5) node[right] {$E_{\text{Vak}}$};
      % Metall
      \draw (1,3.5) -- (1,0);
      \draw (1,1.3) -- (0,1.3) node[left] {$E_F$};
      \node[above] at (.5,0) {ME};
      \draw[<->] (.5,3.5) -- node[phi] {$\phi_{\text{ME}}$} (.5,1.3);
      % n-Halbleiter
      \draw (1.5,3.5) -- (1.5,0);
      \draw (1.5,1) -- (4,1);
      \node[above] at (2.75,0) {n-HL};
      \draw (1.5,2) -- (4,2) coordinate[pin={above right:$E_L$}];
      \draw[DarkOrange3] (1.5,1.8) -- (4,1.8) node[right] {$E_D$};
      \draw (1.5,1.6) -- (4,1.6) coordinate[pin={below right:$E_F$}];
      \draw[<->] (2.75,3.5) -- node[phi] {$\phi_{\text{HL}}$} (2.75,1.6);
      \draw[Purple,<->] (3.2,3.5) -- node[phi] {$\chi$} (3.2,2);
    \end{scope}

    \begin{scope}[shift={(6,-5)}]
      \fill[MidnightBlue!40] (0,0) rectangle (1,1.3);
      \fill[MidnightBlue!20] (1,1) to[out=-60,in=180] (2,.7) -- (4,.7) |- (1,0) -- cycle;
      \draw (0,0) rectangle (4,4);
      \draw (0,3.5) -- (1,3.5) to[out=-60,in=180] (2,3.2) -- (4,3.2) node[right] {$E_{\text{Vak}}$};
      \draw (4,1.3) -- (0,1.3) node[left] {$E_F$};
      % Metall
      \draw (1,1.3) -- (1,0);
      \node[above] at (.5,0) {ME};
      \draw[<->] (.5,3.5) -- node[phi] {$\phi_{\text{ME}}$} (.5,1.3);
      % n-Halbleiter
      \draw (1,1) to[out=-60,in=180] (2,.7) -- (4,.7);
      \node[above] at (2.75,0) {n-HL};
      \draw (1,2) to[out=-60,in=180] (2,1.7) -- (4,1.7) coordinate[pin={above right:$E_L$}];
      \draw[DarkOrange3] (1,1.8) to[out=-60,in=180] (2,1.5) -- (4,1.5) node[right] {$E_D$};
      \draw[<->] (2.75,3.2) -- node[phi] {$\phi_{\text{HL}}$} (2.75,1.3);
      \draw[Purple,<->] (3.2,3.2) -- node[phi] {$\chi$} (3.2,1.7);
      % Barriere
      \draw[MidnightBlue] (1,2) -- (1,1.3);
      \foreach \y in {2,1.7,1.4} \node[MidnightBlue] at (.9,\y) {$+$};
    \end{scope}
  \end{tikzpicture}
  \caption{Kontakt von Metall und n-Halbleiter. In der oberen Zeile
    ist die Situation dargestellt, dass das Fermi-Niveau des Metalls
    über dem des n-Halbleiters liegt, unten sieht man den
    umgekehrten Fall.  Links ist jeweils die Konfiguration vor, rechts
    bei Kontakt abgebildet.  Nach \textcite{hunklinger}.}
  \label{fig:8.3}
\end{figure}

Um ein Elektron aus einem Material zu lösen muss es ins Vakuum
befördert werden.  Die dazu notwendige Energie heißt Austrittsarbeit
und ist gegeben durch die Differenz von Fermi-Niveau und
Vakuum-Niveau, also
\begin{equation*}
  \phi = E_{\text{Vak}} -E_F \; .
\end{equation*}
Die \acct{Elektronenaffinität} ist definiert als der Abstand der
unteren Leitungsbandkante zum Vakuum-Niveau.
\begin{equation*}
  \chi = E_{\text{Vak}} - E_L \; .
\end{equation*}

Metall und Halbleiter haben unterschiedliche Austrittsarbeiten
$\phi_{\text{ME}}$ und $\phi_{\text{HL}}$.  Liegt das Fermi-Niveau des
Metalls höher als das des Halbleiters, also $\phi_{\text{ME}} >
\phi_{\text{HL}}$, so können Elektronen ungehindert vom Metall in den
Halbleiter fließen.  Die Leitfähigkeit wird lediglich durch die
Eigenschaften des Festkörpers limitiert, was sich als ohmscher
Widerstand ausdrückt.  Daher nennt man diese Art des Kontakts
\acct{Ohmscher Kontakt}.

Liegt das Fermi-Niveau des Metalls jedoch tiefer als das des
Halbleiters, also $\phi_{\text{ME}} < \phi_{\text{HL}}$, fließen
Elektronen vom Halbleiter ins Metall.  Dadurch bildet sich am Kontakt
eine Verarmungszone, die durch den Mangel von freien Ladungsträgern
einen großen Widerstand aufweist.  Es entsteht eine Potentialbarriere
der Höhe $\phi_B$.  Der Kontakt wirkt blockierend und wird
\acct{Schottky-Kontakt} genannt.

\minisec{Idealer Fall}

Wir nehmen an, dass die Höhe der Potentialbarriere $\phi_B$ einfach
durch die Differenz der Austrittsarbeiten und den Abstand des
Fermi-Niveaus von der Leitungsbandkante berechnet werden kann.  Es
bilden sich daher keine Grenzflächenzustände am
Metall-Halbleiter-Kontakt.  Für diesen Kontakt gelten einige
Grundregeln:
\begin{enumerate}
\item Ohne Kontakt wird die gegenseitige Energieanordnung durch das
  gemeinsame Vakuumniveau vermittelt.
\item Mit Kontakt ist das Fermi-Niveau in beiden Materialien
  durchgehend konstant.
\item Die Majoritäten bestimmen die Ausgleichsprozesse an der
  Grenzfläche. Daher wird die Barrierenhöhe $\phi_B$ zwischen dem
  metallischen Fermi-Niveau und der Bandkante der Majoritätsträger
  gerechnet.
\end{enumerate}

\minisec{Realer Fall}

Am Metall-Halbleiter-Kontakt bilden sich Grenzflächenzustände mit
hoher Dichte aus. Diese bestimmen die Lage des Fermi-Niveaus. Dies
führt zur Ausbildung einer Barriere, der sogenannten
\acct{Schottky-Barriere}. Diese ist die für den
Metall-Halbleiter-Übergang charakteristische Größe.

\begin{table}[tb]
  \centering
  \begin{tabular}{rl}
    \toprule
    Si & $\phi_B = \num{0.27}\,\phi_{\text{ME}} - (\num{0.55}\pm\SI{0.22}{\eV})$ \\
    GaAs & $\phi_B = \num{0.075}\,\phi_{\text{ME}} - (\num{4.49}\pm\SI{0.24}{\eV})$ \\
    \bottomrule
  \end{tabular}
  \caption{Experimentelle Daten zur Schottky-Barriere.}
  \label{tab:8.1}
\end{table}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: