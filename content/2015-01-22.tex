Kennen wir das lokale Feld können wir mit \eqref{eq:13.34} und
\eqref{eq:13.35} die Frequenz der longitudinalen und transversalen
Phononen bestimmen.  Unter Vernachlässigung der Dämpfung in
\eqref{eq:13.34} findet man
\begin{align}
  \label{eq:13.38}
  \omega_t^2 &= \omega_0^2 - \frac{n q^2}{3 \varepsilon_0 \mu} \frac{1}{1- n\alpha/3} \; , \\
  \label{eq:13.39}
  \omega_\ell^2 &= \omega_0^2 - \frac{2 n q^2}{3 \varepsilon_0 \mu} \frac{1}{1- 2 n\alpha/3} \; .
\end{align}
Das elektrische Feld hebt also die Frequenz der longitudinalen
Schwingungen an und senkt die der transversalen ab, d.h.\ $\omega_\ell
> \omega_t$.  Gitterverzerrung und Polarisation sind in
Ionenkristallen eng miteinander verknüpft.

\subsection{Wechselwirkung zwischen elektromagnetischen Wellen und
  optischen Phononen}

Longitudinal optische Phononen wechselwirken nicht mit
elektromagnetischen Wellen, da die Felder senkrecht aufeinander
stehen.  Transversal optische Phononen wechselwirken jedoch, da die
Felder parallel sind.  Im Infraroten können wir direkte Absorption
erreichen.  Die dielektrische Funktion lautet
\begin{equation*}
  \varepsilon(\omega) = 1 + \chi = 1 + \frac{P}{\varepsilon_0 E}
\end{equation*}
wobei $E$ das mittlere makroskopische Feld ist.  Eliminiere nun
$E_{\text{lok}}(t)$ und $u(t)$ in \eqref{eq:13.35} mit Hilfe von
\eqref{eq:13.18} und \eqref{eq:13.34} dann ergibt sich das Monster
\begin{equation}
  \label{eq:13.40}
  \varepsilon(\omega) = 1 + \underbrace{\frac{n\alpha}{1 -
      \frac{n\alpha}{3}}}_{\mathclap{\text{elektronischer Anteil}}} +
  \underbrace{ \frac{n q^2}{\varepsilon_0 \mu} \left(\frac{1}{1 -
        \frac{n\alpha}{3}}\right)^2 \frac{1}{ \underbrace{\omega_0^2 - \frac{n
          q^2}{3\varepsilon_0\mu (1-n\alpha/3)}}_{\omega_t^2} - \omega^2 -
      \ii\gamma\omega} }_{\text{Ionenbeitrag}} \; .
\end{equation}
Die Resonanz tritt hier nicht bei $\omega_0$, sondern bei $\omega_t$
auf.

Glücklicherweise lässt sich die Gleichung etwas umschreiben
\begin{equation}
  \label{eq:13.41}
  \varepsilon(\omega) = \varepsilon(\infty) +
  \frac{\omega_t^2(\varepsilon_{\text{stat}} -
    \varepsilon(\infty))}{\omega_t^2 - \omega^2 - \ii\gamma\omega} \; .
\end{equation}
Dies bedarf etwas an Erklärung.  $\varepsilon(\infty)$ und
$\varepsilon_{\text{stat}}$ stehen für die Grenzwerte des
Ionenbeitrags für hohe und tiefe Frequenzen.
$\varepsilon_{\text{stat}}$ beinhaltet den Ionen- und
Elektronenanteil.

\begin{notice}
  \enquote{$\infty$} weist nicht auf unendlich hohe Werte, sondern auf
  Frequenzen hin, die groß gegen die Frequenz der Ionenresonanz sind,
  d.h.\ $\varepsilon(\infty)$ spiegelt die elektronische Polarisation
  im Grenzfall kleiner Frequenzen wieder.
\end{notice}

Natürlich zerlegen wir auch diese dielektrische Funktion $\varepsilon
= \varepsilon' + \ii\varepsilon''$ wieder in Real- und Imaginärteil.
\begin{align}
  \label{eq:13.42}
  \Aboxed{
    \varepsilon'(\omega) &= \varepsilon_\infty + \frac{(\varepsilon_{\text{stat}}
      - \varepsilon_\infty) \omega_t^2 (\omega_t^2 - \omega^2)}{(\omega_t^2
      - \omega^2)^2 + \gamma^2\omega^2}
  } \\
  \label{eq:13.43}
  \Aboxed{
    \varepsilon''(\omega) &= \frac{(\varepsilon_{\text{stat}} -
      \varepsilon_\infty) \omega_t^2 \gamma \omega}{(\omega_t^2 -
      \omega^2)^2 + \gamma^2\omega^2}
  }
\end{align}
Division von \eqref{eq:13.38} und \eqref{eq:13.39} ergibt die \acct{Lyddane-Sachs-Teller-Beziehung}
\begin{equation}
  \label{eq:13.44}
  \boxed{
    \frac{\omega_\ell^2}{\omega_t^2} =
    \frac{\varepsilon_{\text{stat}}}{\varepsilon_\infty}
  } \; .
\end{equation}
Sie zeigt, dass dielektrische und elastische Eigenschaften sind eng
miteinander verknüpft.  Man sieht, dass $\varepsilon_{\text{stat}}$
sehr groß wird, wenn sich die Eigenfrequenz der transversalen
optischen Phononen stark verringert.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \def\gamma{.04}
    \def\om{1}
    \begin{axis}[axis on top,domain=.5:2,enlargelimits=false,ymin=-15,ymax=30,width=6cm,
      xlabel={Frequenz $\omega$},ylabel={Amplitude},
      extra y ticks={0},extra y tick labels={$0$},
      extra x ticks={\om,1.41308},extra x tick labels={$\omega_t$,$\omega_\ell$},
      xtick=\empty,ytick=\empty,samples=100,smooth]
      \draw (axis cs:.5,0) -- (axis cs:2,0);
      \draw[dashed] (axis cs:\om,-15) -- (axis cs:\om,30);
      \draw[dashed] (axis cs:1.41308,-15) -- (axis cs:1.41308,30);
      \node[MidnightBlue,above right] at (axis cs:.5,2.33) {$\varepsilon_{\text{stat}}$};
      \node[MidnightBlue,above left] at (axis cs:2,0) {$\varepsilon_\infty$};
      \addplot[MidnightBlue] {1 + (\om^2-x^2)/((\om^2-x^2)^2 + \gamma^2*x^2};
      \addplot[DarkOrange3] {\gamma*x/((\om^2-x^2)^2 + \gamma^2*x^2};
      \legend{$\varepsilon'(\omega)$,$\varepsilon''(\omega)$}
    \end{axis}
  \end{tikzpicture}
  \caption{Schematischer Verlauf der dielektrischen Funktion.  Die
    Nullstellen von $\varepsilon'(\omega)$ liegen bei $\omega_\ell$
    und in der Nähe von $\omega_t$.}
  \label{fig:25.1}
\end{figure}

\subsection{Phonon-Polariton}

Transversal optische Phononen können direkt an elektromagnetische
Wellen ankoppeln.  Eine Mischung der beiden Wellentypen heißt
Polariton.

Als Beispiel betrachten wir eine elektromagnetische Welle, die mit
einer transversal optischen Welle wechselwirkt.  Beide Wellen laufen
in $x$-Richtung und sind in $y$-Richtung polarisiert.
Für die Polarisation des Phonons nehmen wir
\begin{equation*}
  \bm P_t = \hat{\bm y} P_0 \ee^{-\ii(\omega t-k_t x)}
\end{equation*}
an.  Die elektromagnetische Welle wird durch die Wellengleichung
\begin{equation}
  \label{eq:13.45}
  c^2 \nabla^2 E = \varepsilon(\omega) \ddot E
\end{equation}
beschrieben.  Deren Lösung ist nach unseren Voraussetzungen
\begin{equation*}
  \bm E = \hat{\bm y} E_0 \ee^{-\ii(\omega t-k_t x)} \; .
\end{equation*}
Setzen wir die Lösung ein, so erhalten wir die bekannte
Dispersionsrelation
\begin{equation}
  \label{eq:13.46}
  \omega^2 = \frac{1}{\varepsilon(\omega)} c^2 k_t^2 \; .
\end{equation}
Benutzen wir nun $\varepsilon(\omega)$ aus \eqref{eq:13.41} und
vernachlässigen die Dämpfung und setzen außerdem \eqref{eq:13.46} ein
\begin{equation}
  \omega^2 \left[ \varepsilon_\infty +
    \frac{\omega_t^2(\varepsilon_{\text{stat}}-\varepsilon_\infty)}%
    {\omega_t^2-\omega^2}
  \right] = c^2 k_t^2 \; .
\end{equation}
Betrachten wir noch zwei Grenzfälle dieser Gleichung.  Ist $\omega \ll
\omega_t$ bleibt nur $\omega=c k_t/\sqrt{\varepsilon_{\text{stat}}}$
zurück.  Für kleine Frequenzen breitet sich die Welle also mit einer
durch die statische Dielektrizitätskonstante gegebenen Geschwindigkeit
aus.  Ähnliches gilt für große Frequenzen $\omega \gg \omega_t$, dann
bleibt nur $\omega=c k_t/\sqrt{\varepsilon_\infty}$.  Der Mischzustand
bei $\omega=\omega_t,\omega_\ell$ wird Polariton genannt.  Treffen
elektromagnetische Wellen im verbotenen Bereich mit $\omega_t < \omega
< \omega_\ell$ auf die Probe werden sie totalreflektiert.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \fill[MidnightBlue!20] (0,1) rectangle (5,2);
    \draw[<->] (0,4) node[left] {$\omega$} |- (5,0) node[right] {$k_t$};
    \draw[draw=MidnightBlue] (-2pt,2) node[left] {$\omega_\ell$} -- (5,2);
    \draw[draw=MidnightBlue] (-2pt,1) node[left] {$\omega_t$} -- (5,1);
    \draw[dashed] (0,0) -- (3,4) node[right] {$c k_t/\sqrt{\varepsilon_\infty}$};
    \draw[dashed] (0,0) -- (5,4) node[right] {$c k_t/\sqrt{\varepsilon_{\text{stat}}}$};
    \draw[DarkOrange3] (0,2) to[out=0,in=-127] (3,4);
    \draw[DarkOrange3] (0,0) to[in=0] (4,1);
  \end{tikzpicture}
  \caption{Dispersionsrelation des Polaritons (orange).  Die
    Dispersionskurven der Phononen verlaufen waagrecht (dunkelblau),
    die Dispersionskurven des Lichts sind für zwei Grenzfälle
    eingezeichnet (gestrichelt).  Der verbotene Bereich ist hellblau
    hinterlegt.}
  \label{fig:25.2}
\end{figure}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: