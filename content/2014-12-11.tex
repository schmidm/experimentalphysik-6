Als Modell wählen wir eine lineare Kette von identischen Spins.  Als
Vorzugsrichtung wählen wir die $z$-Richtung.  Falls ein Spin mit dem
magnetischen Moment $\mu$ aus der $z$-Richtung ausgelenkt wird tritt
ein Drehmoment auf, das den Spin in die Vorzugsrichtung zurücktreibt
\begin{equation}
  \label{eq:11.43}
  \bm D = \bm \mu \times \bm B_M
\end{equation}
mit $\bm \mu = -g\mu_B \bm S$.  Damit können wir die
Bewegungsgleichung aufstellen
\begin{equation}
  \label{eq:11.44}
  \hbar \frac{\diff \bm S}{\diff t} = - g \mu_B \bm S \times \bm B_M \; .
\end{equation}
Wir berücksichtigen nur Wechselwirkungen zwischen nächsten Nachbarn,
womit sich die Austauschwechselwirkung auf
\begin{equation*}
  U_m = - J\bm S_m(\bm S_{m-1} + \bm S_{m+1})
\end{equation*}
reduziert.  Nach \eqref{eq:11.14} gilt aber ebenfalls
\begin{equation*}
  U_m = - \bm \mu \cdot \bm B_M = g \mu_B \bm S_m \cdot \bm B_M \; .
\end{equation*}
Wir identifizieren $\bm B_M$ durch Vergleich als
\begin{equation}
  \label{eq:11.45}
  \bm B_M = \left( \frac{-J}{g \mu_B}\right) (\bm S_{m-1} + \bm S_{m+1}) \; .
\end{equation}
Nun setzen wir dieses $\bm B_M$ \eqref{eq:11.45} in die
Bewegungsgleichung \eqref{eq:11.44} ein und erhalten damit
\begin{equation}
  \label{eq:11.46}
  \frac{\diff \bm S_m}{\diff t}
  = \frac{J}{\hbar} \bm S_m \times (\bm S_{m-1} + \bm S_{m+1}) \; .
\end{equation}
Für kleine Auslenkungen in $x$- und $y$-Richtung eignet sich ein
Ansatz einer laufenden Welle mit Wellenzahl $k$ und Amplitude $A$.
\begin{equation}
  \label{eq:11.47}
  \begin{aligned}
    S_{m,x} &= A \cos(mka-\omega t) \\
    S_{m,y} &= A \sin(mka-\omega t) \\
    S_{m,z} &= \sqrt{S^2 - A^2}
  \end{aligned}
\end{equation}
Setze den Lösungsansatz \eqref{eq:11.47} in die Bewegungsgleichung
\eqref{eq:11.46} ein.  Wir erhalten daraus die Dispersionsrelation
\begin{equation}
  \label{eq:11.48}
  \boxed{
    \omega = \frac{2 J S}{\hbar} [1-\cos(ka)]
    = \frac{4 J S}{\hbar} \sin^2\left(\frac{ka}{2}\right)
  } \; .
\end{equation}
Diese lässt sich für kleine Wellenvektoren $k$ um $k=0$ entwickeln und
liefert in der zweiten Ordnung
\begin{equation}
  \label{eq:11.49}
  \omega = \frac{JS}{\hbar} a^2 k^2 \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[no marks,domain=0:pi,xmax=4,ymax=2,axis on top,width=5cm,
      enlargelimits=false,xtick=\empty,ytick=\empty,extra x ticks={3.1415},extra x tick labels={$\pi/a$},
      xlabel={Wellenzahl $k$},ylabel={Magnonenfrequenz $\omega$}]
      \draw[dashed] (axis cs:pi,0) -- (axis cs:pi,2);
      \addplot[MidnightBlue] {sin(deg(x/2))^2};
      \addplot[DarkOrange3] {x^2};
      \node[MidnightBlue] at (axis cs:2.7,.7) {$\omega(k)$};
      \node[DarkOrange3] at (axis cs:1.8,1.6) {$\sim k^2$};
    \end{axis}
  \end{tikzpicture} 
  \caption{Dispersionsrelation von Magnonen.  Die dunkelblau Kurve ist
    die nach \eqref{eq:11.48} tatsächlich berechnete Dispersion, die
    orange Kurve ist die Näherung nach \eqref{eq:11.49}.}
  \label{fig:18.1}
\end{figure}

Führt man eine ähnliche Rechnung für ein kubisches Gitter statt einer
linearen Kette durch, so findet man
\begin{equation*}
  \omega = \frac{JS}{\hbar} \sum_{i=1}^z [z - \cos(\bm k \bm r_i)] \; ,
\end{equation*}
wobei die Koordinationszahl $z$ noch frei wählbar ist.

\paragraph{Quantisierung der Spinwelle} (analog wie bei Phononen) Die
Energie einer Mode der Frequenz $\omega_k$ mit $n_k$ Magnonen ist
gegeben durch die Energieniveaus eines harmonischen Oszillators
\begin{equation}
  \label{eq:11.50}
  E_k = \left( n_k + \frac12 \right) \hbar \omega_k \; .
\end{equation}
Die Anregung eines Magnons entspricht dem Umklappen eines Spin $1/2$.

\paragraph{Thermodynamik der Magnonen} Die Zahl der angeregten
Magnonen bestimmt den Temperaturverlauf der Magnetisierung bei tiefen
Temperaturen.  Bei $T=0$ lässt sich die Magnetisierung mit
\eqref{eq:11.20} ausdrücken, wobei die Brillouin-Funktion für $T=0$
gegen Eins geht.  Setzen wir zusätzlich $J=S$, da es in unserem Modell
keinen Bahndrehimpuls gibt, dann gilt
\begin{equation}
  \label{eq:11.51}
  M_S(0) =  n g \mu_B S \; .
\end{equation}
Dies entspricht der Sättigungsmagnetisierung.

Jedes Magnon entspricht dem Unklappen eines Spins, also der Reduktion
des Gesamtspins um $\hbar$ und der Reduktion der Magnetisierung um
$g\mu_B$ unabhängig von der Magnonenenergie $\hbar\omega$.  Damit
können wir den Temperaturverlauf als Abweichung von der
Sättigungsmagnetisierung durch angeregte Magnonen angeben
\begin{equation}
  \label{eq:11.52}
  M_S(T) = M_S(0) - g \mu_B n_{\text{mag}}
\end{equation}
mit der Zahl der angeregten Magnonen pro Volumen $n_{\text{mag}}$.

\paragraph{Berechnung von $n_{\textnormal{mag}}$} Das Vorgehen ist
dasselbe wie bei Phononen in Kapitel 5.  Die Magnonendichte ist also
gegeben durch
\begin{equation*}
  n_{\text{mag}} = \int D(\omega) \braket{n(\omega,T)} \diff\omega
\end{equation*}
mit der Zustandsdichte
\begin{equation*}
  D(\omega) = \frac{V}{2\pi^2} \frac{q^2}{v_g}
  = \frac{V}{4\pi^2} \left( \frac{\hbar}{J S a^2} \right)^{\! 3/2} \sqrt{\omega}
\end{equation*}
und der Bose-Einstein-Statistik $\braket{n(\omega,T)}$.  Nach einiger
aufwändiger Rechnung, die im Buch von \textcite{hunklinger}
nachvollzogen werden kann, ergibt sich
\begin{equation}
  \label{eq:11.53}
  \boxed{
    \frac{M_S(0) - M_S(T)}{M_S(0)} \sim T^{3/2}
  } \; .
\end{equation}
Dies ist das \acct[Blochsches $T^{3/2}$-Gesetz]{Blochsche $T^{3/2}$-Gesetz}.

\paragraph{Nachweis der Magnonen} Man weist Magnonen über
Neutronenstreuexperimente nach, denn ein Neutron sieht zwei Aspekte
eines Kristalls.  Zum einen die Verteilung der Kerne und zum anderen
die Verteilung der magnetischen Momente, da das magnetische Moment des
Neutrons mit dem magnetischen Moment der Atome des Festkröpers
wechselwirkt.  Der Wirkungsquerschnitt der
Neutron-Elektron-Wechselwirkung ist ungefähr von der Ordnung des
Wirkungsquerschnitts der Neutron-Kern-Wechselwirkung.  Für den
Streuprozess gelten Energie- und Impulserhaltung
\begin{align*}
  \bm k_n &= \bm k_n' + \bm k + \bm G \; , \\
  \frac{\hbar^2 k_n^2}{2 M_n} &= \frac{\hbar^2 k_n'^2}{2 M_n} + \hbar \omega_k \; .
\end{align*}
Das Feynman-Diagramm der Streuung ist in Abbildung~\ref{fig:18.2} zu
sehen.  Um die Magnonendispersionsrelation von der
Phononedispersionsrelation zu trennen heizt man den Kristall über
$T_C$ auf, da überhalb von $T_C$ die Spinwellenanregungen
verschwinden.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}
      \draw[->] (150:1.5) node[above] {$\bm k_n$} node[below left] {Neutron} -- (0,0);
      \draw[->] (0,0) -- (30:1.5) node[above] {$\bm k_n'$};
      \draw[decorate,decoration={snake,post length=1mm},->]
      (0,0) -- node[right] {$\bm k$ Magnon} (-90:1.5);
  \end{tikzpicture}
  \caption{Feynman-Diagramm der Neutron-Magnon-Streuung.}
  \label{fig:18.2}
\end{figure}

\paragraph{Ferromagnetische Domänen} Die freie Energie der Probe wird
durch die Bildung von Domänen minimiert.   Die freie Energie setzt sich aus verschiedenen Komponenten zusammen.  Einen Beitrag liefert in jedem Fall die schon oft erwähnte Austauschenergie mit
\begin{equation*}
  E_1 = - J \bm S_1 \bm S_2 \; .
\end{equation*}
\todo[inline]{Ist das richtig?}
Ein weiterer Beitrag stammt von der magnetischen Energie
\begin{equation*}
  E_2 = \frac12 \int \bm H \bm B \diff V \; .
\end{equation*}
Einen weiteren Beitrag liefert die Anisotropieenergie.  Dieser etwas
seltsame Begriff steht für die Variation der Austauschenergie in
nicht-isotropen Kristallen.  Man findet dort experimentell Richtungen
leichter und schwerer Magnetisierung.

Die Austauschenergie ist in Kristallen richtungsabhängig, da das
Überlappintegral der Wellenfunktion richtungsabhängig ist. Der
realisierte Zustand ist der, für den die Summe aller zuvor genannten
Beiträge minimal wird.

\paragraph{Blochwände} Von einem Weißschen Bezirk zum anderen wechselt
die Magnetisierung nicht abrupt, sondern es findet ein stetiger
Übergang der Magnetisierung statt.  Die Spins sind über eine Strecke
von ca.\ 100 Spins kontinuierlich rotiert, vergleiche auch
Abbildung~\ref{fig:18.3}.  Die Blochwände \enquote{kosten} Energie, da
die Spins nicht alle in eine leichte Richtung stehen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \i in {-20,-10,190,200} \node[MidnightBlue] at (\i/50,0) {$\up$};
    \foreach \i in {0,10,...,180} \node[MidnightBlue,rotate=\i] at (\i/50,0) {$\up$};
    \draw[decorate,decoration=brace] (180/50,-.2) -- node[below] {einige 100 Spins} (0,-.2);
  \end{tikzpicture}
  \caption{Der Übergang von einem Weißschen Bezirk zum nächsten findet
    stetig statt.}
  \label{fig:18.3}
\end{figure}

\subsection{Antiferromagnetismus}

Bevor wir mit dem Kapitel Magnetismus abschließen kommen wir noch zu
den beiden unbekannteren Formen des Ferromagnetismus.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node at (1.5,2.5) {Paramagnetismus};
      \draw[<->] (0,2) node[left] {$\chi$} |- (3,0) node[right] {$T$};
      \draw[MidnightBlue] (.2,2) to[out=-80,in=170] (2.5,.2);
      \node[below,text width=3cm] at (1.5,-.5) {%
        \[\chi = C/T\] Curie-Gesetz
      };
    \end{scope}
    \begin{scope}[shift={(4,0)}]
      \node at (1.5,2.5) {Ferromagnetismus};
      \draw[<->] (0,2) node[left] {$\chi$} |- (3,0) node[right] {$T$};
      \draw[dotted] (1.5,2) -- (1.5,-2pt) node[below] {$T_C$};
      \draw[MidnightBlue] (1.6,2) to[out=-80,in=170] (3,.2);
      \node[below,text width=3cm] at (1.5,-.5) {%
        \[\chi = \frac{C}{T-T_C}\]\[T>T_C\] Curie-Weiss-Gesetz
      };
    \end{scope}
    \begin{scope}[shift={(8,0)}]
      \node at (1.5,2.5) {Antiferromagnetismus};
      \draw[<->] (0,2) node[left] {$\chi$} |- (3,0) node[right] {$T$};
      \draw[dotted] (1,2) -- (1,-2pt) node[below] {$T_N$};
      \draw[MidnightBlue] (1,1.5) to[out=-80,in=170] (3,.2);
      \draw[MidnightBlue] (0,0) to[out=30,in=-100] node[below right] {$\chi_\parallel$} (1,1.5);
      \draw[MidnightBlue] (0,1.3) to[out=-10,in=200] node[above] {$\chi_\perp$} (1,1.5);
      \node[below,text width=3.2cm] at (1.5,-.5) {%
        \[\chi = \frac{2 C}{T+\Theta}\]\[T>T_N\]
        $\perp$: $B$-Feld senkrecht zur Spinachse \\
        $\parallel$: $B$-Feld parallel zur Spinachse \\
      };
    \end{scope}
  \end{tikzpicture}
  \caption{Abhängigkeit der Suszeptibilität von der Temperatur für
    die verschiedenen Formen des Magnetismus.}
  \label{fig:18.4}
\end{figure}

Antiferromagnetismus kann experimentell bei \ch{MnO} beobachtet
werden.  Unterhalb der Néel-Temperatur $T_N$ erhält man in der
Neutronenbeugungsaufnahme unterschiedliche Reflexe abhängig von der
Feldrichtung.  Für \ch{MnO} findet man eine Néel-Temperatur von $T_N =
\SI{116}{\K}$.  Darunter misst man abhängig von der Feldrichtung die
Gitterkonstanten $a_1 = \SI{4.45}{\angstrom}$ und $a_2 =
\SI{8.85}{\angstrom}$.  Es sind also zwei unterschiedliche
\ch{Mn}-Untergitter vorhanden.

\subsection{Ferrimagnetismus}

Der Name Ferrimagnetismus leitet sich daher ab, dass diese Form des
Magnetismus bei Ferriten beobachtet wird.  Ferrite besitzen eine
kompliziert aufgebaute Elementarzelle.  Die Ionen der Metalle sitzen
auf nicht äquivalenten Gitterplätzen, wobei die Spins des einen
Untergitters parallel zu einer der Kanten der Elementarzelle , die
Spins des anderen Untergitters entgegengesetzt ausgerichtet sind.
Ferrimagnetismus tritt auf, wenn die Austauschkonstante zwischen zwei
benachbarten magnetischen Momenten unterschiedlicher Sorte negativ
ist.  Dann sind nämlich die Spins der einen Ionen-Sorte denen der
anderen entgegengesetzt.

\todo[inline]{Keine Ahnung was das Folgende bedeuten soll.}
\begin{description}
\item[Eigenschaften:] Hysterese, $M_S$, $T_C$, aber die
  Sättigungsmagnetisierung $M_S$ ist sehr klein.
\item[Ferrite:] \ch{MeFe2D4} = (\ch{Me^{2+}D^{2-}}) (\ch{Fe2^{3+}O3^{2-}}),
  \ch{Me}: zweiwertiges Metall.
\item[Magnedit:] \ch{Fe3O4} = (\ch{Fe^{2+}D^{2-}}) (\ch{Fe2^{3+}O3^{2-}}),
  $T_2 = \SI{858}{\K}$.
  \begin{align*}
    &\ch{Fe^2+} \quad 3d^6 \quad \up\dn\up\up\up\up \quad \text{4 Spins/Ion} \\
    &\ch{Fe^3+} \quad 3d^5 \quad \up\up\up\up\up \quad \text{5 Spins/Ion} \\
  \end{align*}
  Man erwartet für \ch{Fe3O4}: $14\mu_B$, Messung $\sim 4\mu_B$
  \begin{align*}
    \underbrace{\up\up\up\up\up}_{\ch{Fe^3+}} \quad
    \underbrace{\dn\dn\dn\dn\dn}_{\ch{Fe^3+}} \quad
    \underbrace{\up\up\up\up}_{\ch{Fe^2+}}
  \end{align*}
\end{description}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: