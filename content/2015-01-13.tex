\section{Doppelresonanzverfahren}

Mit Hilfe von sogenannten Doppelresonanzverfahren kann man die
Empfindlichkeit und spektrale Auflösung der kennengelernten
Resonanzverfahren erhöhen.  Im Folgenden werden wir zwei dieser
Verfahren kennen lernen.

\minisec{ENDOR}

Die Abkürzung \acct{ENDOR} steht nicht wie vielleicht vermutet für den
Heimatplaneten der Ewoks im Science-Fiction-Epos Star Wars, sondern
für \textsl{electron nuclear double resonance} oder auf Deutsch
Elektron-Kern-Doppel-Resonanz.

Die Elektron-Kern-Doppel-Resonanz kann am besten anhand des
Wasserstoffatoms erläutert werden.  Das Wasserstoffatom besteht aus
einem Elektron und einem Proton.  Im Magnetfeld spaltet das System in
vier Zustände auf, die wir mit $\Uparrow\downarrow$,
$\Uparrow\uparrow$, $\Downarrow\downarrow$ und $\Downarrow\uparrow$
bezeichnen.  Der doppelt gestrichene Pfeil steht für den
Elektronenspin, der andere für den Kernspin.  In
Abbildung~\ref{fig:22.1} ist das Energieniveauschema des
Wasserstoffatoms gezeigt.  Die Übergänge des ESR- und NMR-Spektrums
sind farbig eingezeichnet.  Im ESR-Spektrum sind diejenigen Übergänge
mit $\Delta m_s = \pm 1$ erlaubt, für NMR diejenigen mit $\Delta m_I =
\pm 1$.

\begin{figure}[tb]
  \centering
  \def\l{1}
  \begin{tikzpicture}[gfx]
    \draw (0,0) node[below] {$B_0=0$} -- +(\l,0);
    \draw[dashed] (\l,0) -- (1+\l,1);
    \draw[dashed] (\l,0) -- (1+\l,-1);
    \draw (1+\l,1) node[left=.1cm] {$\Uparrow$} -- (1+2*\l,1);
    \draw (1+\l,-1) node[left=.1cm] {$\Downarrow$} -- (1+2*\l,-1);
    \node[MidnightBlue] at (1+1*\l,0) {$\uparrow B_0$};
    \draw[dashed] (1+2*\l,1) -- (2+2*\l,1.5);
    \draw[dashed] (1+2*\l,1) -- (2+2*\l,.5);
    \draw[dashed] (1+2*\l,-1) -- (2+2*\l,-.5);
    \draw[dashed] (1+2*\l,-1) -- (2+2*\l,-1.5);
    \draw (2+2*\l,1.5) node[left=.1cm] {$\Uparrow\downarrow$} -- (2+3*\l,1.5);
    \draw (2+2*\l,.5) node[left=.1cm] {$\Uparrow\uparrow$} -- (2+3*\l,.5);
    \draw (2+2*\l,-.5) node[left=.1cm] {$\Downarrow\downarrow$} -- (2+3*\l,-.5);
    \draw (2+2*\l,-1.5) node[left=.1cm] {$\Downarrow\uparrow$} -- (2+3*\l,-1.5);
    \draw[dashed] (2+3*\l,1.5) -- (3+3*\l,1.3);
    \draw[dashed] (2+3*\l,.5) -- (3+3*\l,.7);
    \draw[dashed] (2+3*\l,-.5) -- (3+3*\l,-.3);
    \draw[dashed] (2+3*\l,-1.5) -- (3+3*\l,-1.7);
    \draw (3+3*\l,1.3) -- (3+4*\l,1.3) node[right] (n4) {$4$};
    \draw (3+3*\l,.7) -- (3+4*\l,.7) node[right] (n3) {$3$};
    \draw (3+3*\l,-.3) -- (3+4*\l,-.3) node[right] (n2) {$2$};
    \draw (3+3*\l,-1.7) -- (3+4*\l,-1.7) node[right] (n1) {$1$};

    \draw[DarkOrange3,<->] (3.3+3*\l,1.3) -- node[left,pos=.7] {ESR} (3.3+3*\l,-.3);
    \draw[DarkOrange3,<->] (3.5+3*\l,.7) -- (3.5+3*\l,-1.7);
    \draw[MidnightBlue,<->] (3.7+3*\l,1.3) -- (3.7+3*\l,.7);
    \draw[MidnightBlue,<->] (3.7+3*\l,-.3) -- node[right] {NMR} (3.7+3*\l,-1.7);

    \node[above=.2cm] at (4+4*\l,0 |- n4) {$m_S$};
    \node at (4+4*\l,0 |- n4) {$1/2$};
    \node at (4+4*\l,0 |- n3) {$1/2$};
    \node at (4+4*\l,0 |- n2) {$-1/2$};
    \node at (4+4*\l,0 |- n1) {$-1/2$};

    \node[above=.2cm] at (4+5*\l,0 |- n4) {$m_I$};
    \node at (4+5*\l,0 |- n4) {$-1/2$};
    \node at (4+5*\l,0 |- n3) {$1/2$};
    \node at (4+5*\l,0 |- n2) {$-1/2$};
    \node at (4+5*\l,0 |- n1) {$1/2$};

    \node[pin={[text width=2.5cm,align=center]260:%
      Zeeman-Energie des $e^-$ \\ $g_S \mu_B m_S B_0$}] at (2.5,-1) {};
    \node[pin={[text width=2.5cm,align=center]below:%
      Zeeman-Energie des Kerns \\ $-g_I \mu_k m_I B_0$}] at (4.5,-1.5) {};
    \node[pin={[text width=2.5cm,align=center]-80:%
      Hyperfein-WW Kern-$e^-$ \\ $a m_I m_S$}] at (6.5,-1.7) {};
  \end{tikzpicture}
  \caption{Energieniveauschema des Wasserstoffatoms in einem äußeren
    Magnetfeld zur Erläuterung des ENDOR-Verfahrens.  Die Übergange,
    die in den verschiedenen Verfahren ausgenutzt werden sind
    eingezeichnet.}
  \label{fig:22.1}
\end{figure}

Nimmt man das reine ESR-Spektrum auf, so sind die Linien durch die
Wechselwirkungen mit anderen Kernen inhomogen verbreitert.  Ein reines
NMR-Spektrum hat hingegen eine geringe Nachweisempfindlichkeit, da für
NMR immer sehr viele Spins notwendig sind.

Bei ENDOR kombiniert man beide Verfahren und geht wie folgt vor.  Man
sättigt zunächst einen der ESR-Übergänge, wir wählen $1$--$3$, d.h.\
die Intensität der Linie nimmt ab, da durch Einstrahlung resonanter
Mikrowellen die Besetzung in $1$ und $3$ teilweise angeglichen wird.
Gleichzeitig treibt man mit resonanter Strahlung den NMR-Übergang
$3$--$4$.  Es wird also die Besetzung, die mittels ESR in $3$
getrieben wurde von dort nach $4$ gebracht.  Dadurch sinkt die
Besetzung in $3$ wieder und das ESR-Absorptionssignal steigt an wie in
Abbildung~\ref{fig:22.2} skizziert.  Aus der Linienbreite kann man die
Hyperfein-Wechselwirkung ermitteln.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$I_{\text{ESR}}$} |- (3,0) node[right] {$\nu$ NMR};
    \draw[MidnightBlue,looseness=.2] (0,1) -- (.8,1) to[out=0,in=180] (1,2) to[out=0,in=180] (1.2,1)
    -- (1.8,1) to[out=0,in=180] (2,2) to[out=0,in=180] (2.2,1) -- (3,1);
    \draw[dashed] (1,0) node[below] {$3$-$4$} -- (1,2);
    \draw[dashed] (2,0) node[below] {$1$-$2$} -- (2,2);
  \end{tikzpicture}
  \caption{Treiben wir die NMR-Übergänge, so erfährt die
    ESR-Absorption bei diesen Frequenzen ihre Maxima.}
  \label{fig:22.2}
\end{figure}

Vereinfach gesagt: Wir treiben mit ESR von $1$ nach $3$.  Dieser
Übergang sättigt irgendwann weil die Besetzung in $1$ und $3$ sich
angleicht.  Treiben wir mit NMR dann von $3$ nach $4$ vermindern wir
die Besetzung in $3$ und der ESR Übergang findet wieder statt.

\minisec{ODMR}

Ein anderes Doppelresonanzverfahren ist der optische Nachweis der
magnetischen Resonanz (\acct{ODMR}, nach engl.: \textsl{optically detected
  magnetic resonance}).  Man verwendet die Intensität eines im
optischen Spektralbereich liegenden Elektronenübergangs um
Elektronenspin- oder Kernspinübergänge nachzuweisen.  Die ESR- oder
NMR-Übergänge werden durch entsprechende resonante
Mikrowellenstrahlung angeregt.

Wir diskutieren die Funktionsweise anhand eines Beispiels aus der
Molekülphysik.  Organische Moleküle haben einen metastabilen
Triplett-Zustand $T_1$.  Dieser liegt in etwa \SI{20000}{\per\cm} über
dem Grundzustand in den Emission mit langer Lebensdauer, sogenannte
Phosphoreszenz, stattfinden kann.  Durch Dipol-Dipol-Wechselwirkung
der magnetischen Momente von Kern und Elektron kommt es zu einer
Nullfeldaufspaltung der drei möglichen Spinzustände\footnote{Die
  explizite Rechnung kann bei \textcite[409\psqq]{haken-mol}
  nachvollzogen werden.} ($m_S = -1,0,1$).  Aus nicht weiter
spezifizierten Symmetriegründen ist die Emission aus den magnetischen
Unterzuständen $\tau_x$, $\tau_y$ und $\tau_z$ in den Grundzustand
$S_0$ nicht gleich.  Wird während der Beobachtung der Phosphoreszenz
die Population in einen Unterzustand größerer
Emissionswahrscheinlichkeit gepumpt, so nimmt die Phosphoreszenz zu.
So können wir durch Beobachtung der Veränderung der Emission
Elektron-Resonanz nachweisen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \draw (0,3) node[left] {$S_1$} -- (1,3);
      \draw (1.5,2) node[left] {$T_1$} -- (2.5,2);
      \draw (0,0) node[left] {$S_0$} -- (1,0);
      \draw[dashed] (1,3) -- (1.5,2);
      \draw[MidnightBlue,->] (2,2) -- node[below right,text width=2.5cm] {Phosphoreszenz ca.\ \SI{20000}{\per\cm} (grün)} (.5,0);
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \draw (.5,1.75) ellipse (.6 and 1.3);
      \node[above] at (.5,1.75+1.3) {ESR};
      \draw[Purple] (0,2.5) -- (1,2.5) node[right] {$\tau_x$};
      \draw[Purple] (0,2) -- (1,2) node[right] {$\tau_y$};
      \draw[Purple] (0,1) -- (1,1) node[right] {$\tau_z$};
      \draw[DarkOrange3,<->] (.3,2.5) -- (.3,2);
      \draw[DarkOrange3,<->] (.6,2.5) -- (.6,1);
      \draw[MidnightBlue,->] (.6,2.5) -- node[pos=.9,below right,text width=2.5cm] {Phosphoreszenz selektiv aus $\tau_x$ (Auswahlregeln)} (-.5,0);
    \end{scope}
    \begin{scope}[shift={(8,0)}]
      \draw (0,3) node[left] {$E$} |- (3.5,0) node[below left] {Besetzungszahlen};
      \draw[name path=A] (1,3) to[out=-80,in=160] (3,.5);
      \path[name path=B] (0,2.5) -- (4,2.5);
      \draw[Purple,name intersections={of=A and B,name=i}] (0,2.5) -- (i-1) node[right] {$N_x$};
      \path[name path=B] (0,2) -- (4,2);
      \draw[Purple,name intersections={of=A and B,name=i}] (0,2) -- (i-1) node[right] {$N_y$};
      \path[name path=B] (0,1) -- (4,1);
      \draw[Purple,name intersections={of=A and B,name=i}] (0,1) -- (i-1) node[right] {$N_z$};
    \end{scope}
  \end{tikzpicture}
  \caption{Energieniveauschema für $B_0 = 0$ bei ODMR.}
  \label{fig:22.3}
\end{figure}

Die Phosphoreszenzintensität des Prozesses in
Abbildung~\ref{fig:22.3} ist also von der strahlenden
Übergangswahrscheinlichkeit $K_x$ und von den zugehörigen Besetzungen
$N_x$ abhängig
\begin{equation*}
  I(\text{Phos}) \sim K_x \cdot N_X
\end{equation*}
Die Besetzungszahl $N_x$ wird bei Sättigung der ESR-Übergänge erhöht,
folglich nimmt $I(\text{Phos})$ zu.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,2) node[left] {$I(\text{Phos})$} |- (4,0) node[right] {$\nu$ [\si{\GHz}]};
    \draw[MidnightBlue,looseness=.2] (0,1) -- (.8,1) to[out=0,in=180] (1,2) to[out=0,in=180] (1.2,1)
    -- (2.8,1) to[out=0,in=180] (3,2) to[out=0,in=180] (3.2,1) -- (4,1);
    \draw[dashed] (1,0) node[below] {$\tau_x\to\tau_y$} -- (1,2);
    \draw[dashed] (3,0) node[below] {$\tau_x\to\tau_z$} -- (3,2);
  \end{tikzpicture}
  \caption{Erhöhen wir durch Treiben eines ESR-Übergangs die Besetzung
    im emissionsstarken Unterzustand, so beobachten wir eine Zunahme
    der Intensität in der Phosphoreszenz.}
  \label{fig:22.4}
\end{figure}

Ein weiteres Anwendungbeispiel der ODMR ist das
Stickstoff-Fehlstellenzentrum (NV"=Zentrum, von engl.\
\textsl{nitrogen vacancy}) im Diamant.  Dieses hat $S=1$ und besitzt
ein Triplett im Grund- und angeregten Zustand.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw (0,3) node[left] {$^3E$} -- (1,3);
    \draw (1.5,2) -- (2.5,2) node[right] {Singulett};
    \draw (0,0) node[left] {$^3G$} -- (1,0);
    \draw[dashed] (1,3) -- (1.5,2);
    \draw[MidnightBlue,->] (2,2) node[below right] {Übergang in $m_S = 0$} -- (.5,0);
    \draw[DarkOrange3,->] (.5,3) -- node[pos=.4,left] {$\tau\sim\SI{13}{\ms}$} node[pos=.6,left] {$\SI{637}{\nm}$} (.5,0);
    \begin{scope}[shift={(1.5,-.5)}]
      \draw (0,1) -- (1,1) node[right] {$m_S = 1$};
      \draw (0,.75) -- (1,.75) node[right] {$m_S = -1$};
      \draw (0,0) -- (1,0) node[right] {$m_S = 0$};
      \draw[DarkOrange3,<->] (.5,.75) -- node[right] {\SI{2.87}{\GHz} ESR-Übergang} (.5,0);
    \end{scope}
  \end{tikzpicture}
  \caption{Aus dem metastabilen Singulett eines NV-Zentrums ist der
    Fluoreszenz-Übergang von ${m_S=0}$ in den ${m_S=0}$ Grundzustand
    möglich.  Entvölkert man ${m_S=0}$ im Singulett nimmt die
    Fluoreszenz ab.}
  \label{fig:22.5}
\end{figure}

Aus dem angeregten Zustand heraus gibt es zwei Möglichkeiten, wie auch
in Figur~\ref{fig:22.5} eingezeichnet.  Zum einen kann ein strahlender
Übergang mit \SI{637}{\nm} in den Grundzustand stattfinden oder ein
strahlungsloser Übergang zum metastabilen Singulett. Vom $m_S=0$
Zustand des metastabilen Singulett ist ein optischer Übergang zum
$m_S=0$ Grundzustand möglich.  Entvölkert man durch Treiben des in
Abbildung~\ref{fig:22.5} hervorgehobenen ESR-Übergangs das $m_S=0$
Niveau des Singuletts so wird die Fluoreszenz erniedrigt.  Dies macht
sich in unserem ODMR-Signal bemerkbar.

Durch Kombination von ODMR und konfokaler Mikroskopie wird die
Detektion von nur einem NV-Zentrum ermöglicht.  Damit ist die
Beobachtung der Manipulation eines einzelnen Spins möglich.  Dies
findet Anwendungen in den Quanteninformationstechnologien und
Anwendungen als Magnetfeldsensor.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: