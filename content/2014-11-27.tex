\chapter{Festkörpermagnetismus}

Es gibt verschiedene Arten von magnetischen Erscheinungen.
\begin{description}
\item[Diamagnetismus] Es liegen keine ungepaarten Elektronen in
  Atomen, Ionen oder Molekülen des Materials vor.
\item[Paramagnetismus] Es existieren isolierte ungepaarte Elektronen
  in Atomen, Ionen oder Molekülen des Materials.  Ihre Ausrichtung
  erfolgt parallel zum äußeren Magnetfeld.  Wird das äußere Magnetfeld
  entfernt bricht das innere Magnetfeld durch thermische Bewegung
  wieder zusammen.
\item[Ferromagnetismus] Die magnetischen Momente einzelner Teilchen
  sind nicht unabhängig voneinander.  Durch Kopplung findet eine
  parallele Ausrichtung innerhalb kleiner Bereiche (Weisssche Bezirke)
  statt.
\item[Ferrimagnetismus] Es gibt zwei Arten von magnetischen Zentren,
  die nicht unabhängig sind.  Die Spinmomente gleichartiger Zentren
  richten sich parallel aus, die verschiedener antiparallel.  Es
  findet daher eine (partielle) Auslöschung der magnetischen Momente
  statt.
\item[Antiferromagnetismus] Die magnetischen Momente richten sich
  sponten antiparallel aus.  Ein idealer Antiferromagnet hat also kein
  magnetisches Moment.  Mit steigender Temperatur ergibt sich das
  Verhalten eines Ferrimagneten, überhalb der Nèel-Temperatur das
  eines Paramagnets.
\end{description}

Allgemein gilt, dass die Bahnbewegung und der Spin der Elektronen
durch ein magnetisches Moment $\mu$ verknüpft sind.  Des weiteren
können die magnetischen Momente durch ein äußeres Magnetfeld $B_a$
ausgerichtet werden.  Der Beitrag der Kerne zum magnetischen Moment
ist vergleichsweise klein und spielt in der Festkörperphysik nur in
besonderen Fällen eine Rolle, zum Beispiel in der
Tieftemperaturphysik, der Kernspinspektroskopie und der Quantenphysik
mit einzelnen Elektronen im Festkörper.  Das Kernmagneton ist ungefähr
\begin{equation*}
  \mu_k \sim \num{e-3}\,\mu_e
\end{equation*}
mit dem magnetischen Moment $\mu_e$ des Elektrons, auch Bohrsches
Magneton genannt.

\section{Bezeichnungen und Begriffe}

Zwischen der magnetischen Feldstärke (\si{\A\per\m}) und der
magnetischen Induktion (\si{\V\s\per\square\m}) besteht im
materiefreien Raum der folgende Zusammenhang
\begin{equation*}
  \bm B = \mu_0 \bm H
  \;,\quad\text{mit }
  \mu_0 = 4 \pi \cdot \SI{e-7}{\V\s\per\A\per\m} \; .
\end{equation*}
Im materieerfülllten Raum werden durch das Magnetfeld Dipole induziert
(Diamagnetismus) oder vorhandene permanente Dipole parallel zum Feld
ausgerichtet (Paramagnetismus).  Das resultierende magnetische Moment
pro Volumeneinheit ist die Magnetisierung $\bm M$ (\si{\A\per\m}).
Die Magnetisierung $\bm M$ liefert einen zusätzlichen Beitrag zur
magnetischen Induktion.
\begin{equation*}
  \bm B = \mu_0 \bm H + \mu_0 \bm M \; .
\end{equation*}
Sei das äußere Feld durch $\bm H = \bm B_a/\mu_0$ gegeben, dann gilt
\begin{equation}
  \label{eq:11.1}
  \bm B = \bm B_a + \mu_0 \bm M = \mu_0 (\bm H+\bm M) \; .
\end{equation}
Die Magnetisierung $\bm M$ ist als das magnetische Moment pro
Volumeneinheit definiert
\begin{equation}
  \label{eq:11.2}
  \bm M = \frac{\bm m}{V} = N \frac{\bar{\bm\mu}}{V} = n \bar{\bm\mu}
\end{equation}
mit der Zahl der magnetischen Dipole $N$, der Anzahldichte $n$ der
Dipole und dem mittleren Dipolmoment $\bar{\bm\mu}$.  Die magnetischen
Eigenschaften einer Probe werden durch die magnetische Suszeptibilität
$\chi$ bestimmt
\begin{equation}
  \label{eq:11.3}
  \bm M = \chi \bm H \; .
\end{equation}
Die Suszeptibilität $\chi$ ist im Allgemeinen ein Tensor.  Im Weiteren
nehmen wir jedoch isotrope Materialien an und können $\chi$ als Skalar
verwenden.  Die magnetische Permeabilität ist definiert als
\begin{equation*}
  \mu_r = 1 + \chi \; .
\end{equation*}
Wir gehen nicht auf das lokale Magnetfeld ein, das tatsächlich am Ort
des magnetischen Dipols herrscht.  Das lokale Feld unterscheidet sich
nämlich aus zwei Gründen.  Zum einen gibt es das
Entmagentisierungsfeld, welches abhängig von der Probenform ist und
das Feld im Inneren des Festkörpers verändert
\begin{equation*}
  \bm B = \bm B_a - N \bm M
\end{equation*}
mit dem probenformabhängigen Entmagnetisierungsfaktor $N$.  Im
Beispiel eines langen Drahtes ist $N = 0$.  Desweiteren beeinflussen
benachbarte magnetische Momente das lokale Feld.  Die Beziehung
\begin{equation*}
  \bm B_{\text{lok}} \approx \bm B \approx B_a
\end{equation*}
gilt dabei für Dia- und Paramagnete, aber nicht im elektrischen Fall
und nicht für Ferromagnete.

\section{Klassifizierung}

Weiter oben wurden bereits die verschiedenen Formen des Magnetismus
vorgestellt.  Auf einige wollen wir hier näher eingehen.

\paragraph{Diamagnetismus}
Der Diamagnetismus zeichnet sich durch ein induziertes magnetisches
Moment $\bm M$ aus, welches dem Magnetfeld entgegen gerichtet ist.
Daher werden Diamagnetische Stoffe aus einem inhomogenen Magnetfeld
verdrängt.  Für die spezifische diamagnetische Suszeptibilität gilt
\begin{equation*}
  \chi_{\text{dia}} < 0
  \;,\quad
  \lvert\chi_{\text{dia}}\rvert \ll 1
  \quad (\approx \num{e-6})
\end{equation*}
und ist unabhängig von der Temperatur und dem angelegten Magnetfeld.
Der Diamagnetismus ist eine allgemeine Eigenschaft aller Stoffe, er
wird bei para- und ferromagnetischen Stoffen lediglich durch andere
Erscheinungen überdeckt.

\paragraph{Paramagnetismus}
Beim Paramagnetismus ist das induzierte magnetische Moment dem Feld
gleichgerichtet.  Vorhandene permanente Dipole werden ebenfalls
parallel zum Feld ausgerichtet.  Deshalb werden paramagnetische Stoffe
in ein inhomogenes Magnetfeld hineingezogen.  Für die spezifische
paramagnetische Suszeptibilität gilt
\begin{equation*}
  \chi_{\text{para}} > 0
  \;,\quad
  \lvert\chi_{\text{dia}}\rvert < \lvert\chi_{\text{para}}\rvert \ll 1
  \quad (\approx \num{e-4}\ldots\num{e-5})
\end{equation*}
und ist unabhängig vom angelegten Magnetfeld.  Sie ist jedoch
temperaturabhängig und folgt dem Curieschen Gesetz
\begin{equation*}
  \chi_{\text{para}}(T) = \frac{\const}{T} \; .
\end{equation*}

\paragraph{Ferromagnetismus}
Im Gegensatz zu den beiden vorherigen hängt beim Ferromagneten die
Magnetisierung von der Vorgeschichte des Materials ab.  Für die
spezifische ferromagnetische Suszeptibilität gilt
\begin{equation*}
  \chi_{\text{ferro}} > 0
  \;,\quad
  \lvert\chi_{\text{ferro}}\rvert \gg 1
\end{equation*}
und hängt von der Temperatur und dem angelegten Magnetfeld ab.  Wird
der Ferromagnet über die Curie-Temperatur erhitzt, so verliert er
seine ferromagnetischen Eigenschaften und wird paramagnetisch.  In
einem äußeren Feld ist die Magnetisierung $\bm M$ dem Feld $\bm H$
gleichgerichtet.  Ist das Material vollkommen entmagnetisiert (durch
Ausglühen) so nimmt $\bm M$ zunächst linear mit dem angelegten Feld
$\bm B_a$ zu (Kurve a in Abb.~\ref{fig:14.1}) und geht in Sättigung
über, wobei $\bm M_S$ die Sättigungsmagnetisierung bezeichnet.  Wird
$\bm B_a$ reduziert, so wird $\bm M$ nicht Null bei $B_a =0$, sondern
es verbleibt ein Remanenzfeld $\bm M_R$.  Um $\bm M = 0$ zu erreichen,
muss ein Koerzitivfeld $B_K$ angelegt werden.  Die Fläche zwischen den
Kurven (Kurven b und c in Abb.~\ref{fig:14.1}) gibt die aufzuwendenede
Energie an, um einen gesamten Magnetisierungszyklus zu durchlaufen.
Einige Beispielwerte für die Sättigungsmagnetisierung und die
Curie-Temperatur sind in Tabelle~\ref{tab:14.1} aufgelistet.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[name path=x,->] (-3,0) -- (3.5,0) node[below] {$B$};
    \draw[name path=y,->] (0,-2) -- (0,2) node[left] {$M$};
    \draw[name path=c,MidnightBlue,arrow inside={pos=.7}]
    (-3,-1.5) -- (-2,-1.5) to[out=0,in=-135] (.5,0) to[out=45,in=180] node[below,pos=.35] {c} (3.5,1.5);
    \draw[name path=b,MidnightBlue,arrow inside={end=<,pos=.7}]
    (-3,-1.5) to[out=0,in=-135] (-.5,0) to[out=45,in=180] node[above,pos=.6] {b} (2,1.5) -- (3.5,1.5);
    \draw[name path=a,DarkOrange3,arrow inside={pos=.3}] (0,0) to[out=45,in=180] node[above,pos=.3] {a} (2.5,1.5) -- (3.5,1.5);
    \draw[name intersections={of=b and y,name=i}] (i-1) +(2pt,0) -- +(-2pt,0) node[left] {$M_R$};
    \draw[name intersections={of=b and x,name=i}] (i-1) +(0,2pt) -- +(0,-2pt) node[below] {$B_K$};
    \draw[<->] (3.25,1.5) -- node[left] {$M_S$} (3.25,0);
  \end{tikzpicture}
  \caption{Hysteresekurve eines Ferromagneten.}
  \label{fig:14.1}
\end{figure}

\begin{table}[tb]
  \centering
  \begin{tabular}{rS[table-format=4.0]S[table-format=4.0]}
    \toprule
    & {$M_S$ [\si{G}] (bei \SI{300}{\K})} & {$T_C$ [\si{\K}]} \\
    \midrule
    \ch{Fe} & 1707 & 1043 \\
    \ch{Co} & 1400 & 1388 \\
    \ch{Ni} & 485  & 627  \\
    \bottomrule
  \end{tabular}
  \caption{Beispielwerte für die Sättigungsmagnetisierung $M_S$ und
    die Curie-Temperatur $T_C$ eines Ferromagneten.}
  \label{tab:14.1}
\end{table}

\section{Diamagnetismus}

Nach der ersten Hundschen Regel ist der Gesamtdrehimpuls von vollen
Schalen und Unterschalen Null.  Sind also alle Schalen eines Atoms
gefüllt kann es kein permanentes magnetisches Moment haben.  Ein
magnetisches Moment $\mu$ wird durch ein angelegtes Feld $H$
induziert.  Nach der Lenzschen Regel wirkt das induzierte Dipolmoment
seiner Ursache entgegen und richtet sich antiparallel zu $H$ aus.

Die Herleitung der spezifischen diamagnetischen Suszeptibilität über
eine klassische und eine quantenmechanische Behandlung liefern
dasselbe Resultat
\begin{equation*}
  \chi_{\text{dia}} = - \frac{n \mu_0 e^2}{6 m_e} Z \braket{r^2}
\end{equation*}
wobei $Z$ die Elektronenzahl, $\braket{r^2}$ das mittlere
Abstandsquadrat der Elektronen und $m_e$ die Elektronenmasse ist.

\paragraph{Klassische Herleitung}
Zur klassischen Herleitung betrachten wir das Larmor-Theorem für die
Präzession eines Elektrons im Magnetfeld.  Die Larmor-Frequenz ist gegeben durch
\begin{equation}
  \label{eq:11.4}
  \omega_L = \frac{e B}{2m} \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \shade[ball color=white] (0,0,0) circle (1);
    \node at (0,0,0) {Kern};
    \begin{scope}[x={(-20:1cm)},y={(200:1cm)},z={(90:1cm)}]
      \draw[->] (0.707,0,0) -- (2,0,0) node[below] {$x$};
      \draw[->] (0,0.707,0) -- (0,2,0) node[below] {$y$};
      \draw[->] (0,0,0.707) -- (0,0,2) node[above] {$z$};
      \draw[MidnightBlue,<-] (.5,0,1.5) arc(0:360:.5);
      \draw[MidnightBlue,->] (.5,0,-1.5) arc(0:360:.5);
      \node[MidnightBlue,below right] at (.5,0,1.5) {$\omega_0$};
      \node[MidnightBlue,below right] at (.5,0,-1.5) {$\omega_0$};
    \end{scope}
  \end{tikzpicture}
  \caption{Veranschaulichung zum Larmorschen Theorem.  Die
    verschiedenen Elektronen umkreisen den Kern mit unterschiedlicher
    Chiralität.}
  \label{fig:14.2}
\end{figure}

Wir nehmen an, dass die Elektronen den Atomkern mit der Frequenz
$\omega_0$ umkreisen, aber mit unterschiedlicher Chiralität.  Schalten
wir nun das Magnetfeld ein, so werden rechtslaufende Elektronen um
$\omega_L$ beschleunigt und linkslaufende um $\omega_L$ abgebremst.
Da es keinen Widerstand gibt fließt der Strom solange wie das Feld
eingeschaltet ist.  Die Larmor-Präzession von $z$-Elektronen ist also
gleichbedeutend mit einem elektrischen Zusatzstrom $I$, der sich wie
folgt berechnet
\begin{equation}
  \label{eq:11.5}
  I = \text{Ladung} \times \text{Umläufe pro Zeiteinheit}
  = -Z e \left( \frac{\pi}{2} \frac{e B}{2 m_e} \right) \; .
\end{equation}
Für das magnetische Moment gilt
\begin{equation}
  \label{eq:11.6}
  \mu = I \cdot A \; .
\end{equation}
Auf einer Schleife mit dem Radius $\varrho$ wie dem Elektronenorbit
gilt $A = \pi \varrho^2$.  Damit haben wir
\begin{equation}
  \label{eq:11.7}
  \mu = - \frac{Z e^2 B}{4 m_e} \braket{\varrho^2}
\end{equation}
wobei $\braket{\varrho^2} = \braket{x^2} + \braket{y^2}$ das mittlere
Abstandsquadrat des Elektrons senkrecht zur Feldachse des Kerns ist.
Das mittlere Abstandsquadrat beinhaltet natürlich alle Raumrichtungen
\begin{equation*}
  \braket{r^2} = \braket{x^2} + \braket{y^2} + \braket{z^2} \; .
\end{equation*}
Auf Grund der Kugelsymmetrie gilt jedoch $\braket{x^2} = \braket{y^2}
= \braket{z^2}$ und damit
\begin{equation*}
  \braket{r^2} = \frac{2}{3} \braket{\varrho^2}
\end{equation*}
was durch Einsetzen in \eqref{eq:11.7}
\begin{equation*}
  \mu = - \frac{Z e^2 B}{6 m_e} \braket{r^2}
\end{equation*}
ergibt.  Mit $Z=n\mu_0$ gilt dann für die spezifische diamagnetische
Suszeptibilität
\begin{equation}
  \label{eq:11.8}
  \chi_{\text{dia}} = - \frac{n \mu_0 e^2}{6 m_e} Z \braket{r^2} \; .
\end{equation}

\paragraph{Quantenmechanische Herleitung}
Die quantenmechanische Herleitung verläuft wie immer weniger
willkürlich und nachvollziehbarer.  Betrachten wir den kinetischen
Anteil des $N$-Teilchen Hamiltonoperators mit minimaler Kopplung an
das Magnetfeld
\begin{equation*}
  H_{\text{Kin}}
  = \frac{1}{2m} \sum_i (\bm p_i + e \bm A_i)^2 \; .
\end{equation*}
Unter Annahme eines homogenen Feldes in Coulomb-Eichung ($\div\bm
A=0$) gilt $\bm A = -\bm r\times\bm B/2$ und damit
\begin{equation*}
  H_{\text{Kin}}
  = \frac{1}{2m} \sum_i \left(\bm p_i + \frac{e}{2} \bm r_i \times \bm B\right)^2 \; .
\end{equation*}
Sei nun das Magnetfeld in $z$-Richtung ausgerichtet.
\begin{equation*}
  H_{\text{Kin}}
  = \frac{1}{2m} \sum_i \bm p_i^2
  + \frac{e}{2m} \sum_i (\bm r_i\times\bm p_i)_z B_z
  + \frac{e^2 B_z^2}{8 m} \sum_i (x_i^2 + y_i^2) \; .
\end{equation*}
Wir identifizieren $L^z_i = (\bm r_i\times\bm p_i)_z$ und erhalten für
die spezifische Suszeptibilität
\begin{equation*}
  \bm\mu = - \frac{\partial E}{\partial B_z}
  = - \frac{\partial \braket{\varphi|H|\varphi}}{\partial B_z}
  = \underbrace{- \mu_B
    \biggl\langle \varphi \bigg| \sum_i L^z_i \bigg| \varphi \biggr\rangle
  }_{\text{Paramagnetismus}}
  \underbrace{- \frac{e^2 B_z}{4 m}
    \biggl\langle \varphi \bigg| \sum_i x_i^2 + y_i^2 \bigg| \varphi \biggr\rangle
  }_{\text{Diamagnetismus}}
\end{equation*}
Wir verwenden auch hier die Kugelsymmetrie des Problems, um den
diamagentischen Term zu vereinfachen
\begin{equation*}
  \braket{\varphi|x_i^2|\varphi} = \braket{\varphi|y_i^2|\varphi}
  = \frac{1}{3} \braket{\varphi|\bm r_i^2|\varphi}
\end{equation*}
und damit
\begin{equation*}
  \tag{\ref{eq:11.8}}
  \chi_{\text{dia}}
  = - \frac{n \mu_0 e^2}{6 m_e} \sum_i \braket{\varphi|\bm r_i^2|\varphi}
  = - \frac{n \mu_0 e^2}{6 m_e} Z \braket{\bm r^2} \; .
\end{equation*}
Wie bereits zu Beginn des Kapitels bemerkt ist $\chi_{\text{dia}}<0$,
nicht abhängig von der Temperatur.  Zusätzlich sehen wir jetzt, dass
$\chi_{\text{dia}}$ proportional zur Elektronenzahl $Z$ ist.  Im
Prinzip sind alle Substanzen diamagnetisch, wenn man sie auf ihre
inneren Schalen reduziert.  Edelgaskristalle sind immer diamagnetisch,
da bei Edelgasen alle Schalen gefüllt sind.  Daher sind die
diamagnetischen Eigenschaften gut messbar.  Die Gleichung
\eqref{eq:11.8} gilt nicht für kovalente oder gemischt
kovalent-ionische Festkörper, weil sich dort die Bindungelektronen
bevorzugt zwischen den Atomrümpfen aufhalten und durch diese
Anisotropie die Kugelsymmetrie verletzt wird.

In Metallen hat man zudem freie Elektronen.  Eine zusätzliche
Behandlung derer führt auf den Landau-Diamagnetismus, dessen
Herleitung extrem aufwändig ist und daher hier ausgelassen wird.
\begin{equation}
  \label{eq:11.9}
  \chi_{\text{dia}} = \frac{-n\mu_0\mu_B^2}{2 E_F} \left(\frac{m_e}{m^*}\right)^2
  = - \frac{1}{3} \mu_0 \mu_B^2 D(E_F) \left(\frac{m_e}{m^*}\right)^2
\end{equation}
mit der effektiven Masse $m^*$ der Elektronen.  Ist $m^*=m_e$, so
kompensiert der Landau-Magnetismus zu einem Drittel den
Pauli-Paramagnetismus der freien Elektronen, siehe dazu später den
Abschnitt zu Paramagnetismus.

\begin{table}[tb]
  \centering
  \begin{tabular}{%
      rS[table-format=-3.0]@{\qquad}%
      rS[table-format=-4.0]@{\qquad}%
      rS[table-format=-4.0]}
    \toprule
    \ch{He} &  -24 & \ch{Li^+} &   -88 & \ch{ F^-   } & -1181 \\
    \ch{Ne} &  -85 & \ch{Na^+} &  -767 & \ch{Cl^-   } & -3042 \\
    \ch{Ar} & -246 & \ch{ K^+} & -1835 & \ch{Br^-   } & -4437 \\
    \ch{Kr} & -362 & \ch{Rb^+} & -2765 & \ch{ J^-   } & -6360 \\
    \ch{Xe} & -552 & \ch{Cs^+} & -4412 & \ch{Mg^{2+}} &  -541 \\
    \bottomrule
  \end{tabular}
   \caption{Molare diamagnetische Suszeptibilität $\chi_{\text{dia}}$
     (in \SI{e-6}{\cubic\cm\per\mol}) verschiedener Edelgase und einiger
     ausgewählter Ionen, nach \textcite{hunklinger}.}
  \label{tab:14.2}
\end{table}

\section{Paramagnetismus}

Paramagnetismus kann auftreten, wenn Gitterbausteine mit nicht
abgeschlossener Elektronenkonfiguration vorhanden sind.  Der
Gesamtspin ist dann ungleich Null.  Beispiele sind ungepaarte
Elektronen in organischen Radiaklen, Ionen seltener Erden (z.B.\
\ch{Ho^{3+}}), Übergangsmetallionen (z.B.\ \ch{Cr^{3+}}) oder Metalle
(z.B.\ \ch{Na}).

\minisec{Quantenmechanik des Drehimpulses}

Ist $\bm J$ der Gesamtdrehimpuls, dann ist das magnetische Moment
$\mu$ über die Relation
\begin{equation}
  \label{eq:11.10}
  \bm \mu = - g \mu_B \bm J
\end{equation}
mit ihm verknüpft, wobei $g$ der \acct{Landé-Faktor} und $\mu_B$ das
Bohrsche Magneton ist.  Der Landé-Faktor ist formal definiert als
\begin{equation}
  \label{eq:11.11}
  g_J = 1 + \frac{J(J+1)+S(S+1)-L(L+1)}{2J(J+1)}
\end{equation}
mit den Quantenzahlen des Gesamtdrehimpulses $J$, des Bahndrehimpulses
$L$ und des Spins $S$.  Wählt man $z$ als ausgezeichnete Richtung der
Drehimpulsalgebra gilt
\begin{equation}
  \label{eq:11.12}
  J_z = \hbar m_J
  \;,\quad m_J = -J,-J+1,\ldots,J \; .
\end{equation}
Folglich hat das magnetische Moment in $z$-Richtung dieselben
diskreten Einstellmöglichkeiten
\begin{equation}
  \label{eq:11.13}
  \mu_z = - g_J \mu_B m_J \; .
\end{equation}
Die potentielle Energie eines Dipols in einem äußeren Magnetfeld ist
gegeben durch
\begin{equation}
  \label{eq:11.14}
  E_{\text{pot}} = - \bm \mu \cdot \bm B \; .
\end{equation}
Zeigt das Magnetfeld in $z$-Richtung, also $\bm B = (0,0,B_z)$, dann
gilt
\begin{equation}
  \label{eq:11.15}
  E_{\text{pot}} = - \mu_z B_z = g_J \mu_B m_J B_z \; .
\end{equation}
Der Temperaturverlauf der Suszeptibilität wird durch die thermische
Besetzung der Energieniveaus bestimmt.

Zur Berechnung des mittleren magnetischen Moments bilden wir den
Ensemble-Mittelwert von $\mu_z$ mit Hilfe der Boltzmann-Verteilung
\begin{equation}
  \label{eq:11.16}
  \bar\mu_z
  = \frac{\sum_{m_J} \mu_z \ee^{-E_{\text{pot}}/\kB T}}{\sum_{m_J} \ee^{-E_{\text{pot}}/\kB T}}
  = g_J \mu_B \frac{\sum_{m_J} m_J \ee^{-(g_J \mu_B m_J B_z)/\kB T}}{\sum_{m_J} \ee^{-(g_J \mu_B m_J B_z)/\kB T}} \; .
\end{equation}
Die Ausführung der Summe ist nicht sonderlich schwierig, aber zu lang
um sie hier zu präsentieren, weshalb wir hier nur das Ergebnis zeigen
\begin{equation}
  \label{eq:11.17}
  \bar\mu_z = g_J \mu_B J \mathcal B(x)
\end{equation}
mit
\begin{equation}
  \label{eq:11.18}
  x = \frac{g_J \mu_B J B_z}{\kB T} \; .
\end{equation}
Hier ist $\mathcal B(x)$ die \acct{Brillouin-Funktion}
\begin{equation}
  \label{eq:11.19}
  \mathcal B(x) = \frac{2J+1}{2J} \coth\left(\frac{(2J+1)x}{2J}\right)
  - \frac{1}{2J} \coth\left(\frac{x}{2J}\right) \; .
\end{equation}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: