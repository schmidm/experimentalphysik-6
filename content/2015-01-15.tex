\chapter{Dielektrische und optische Eigenschaften}

Die Wechselwirkung zwischen elektromagnetischen Feldern und
Festkörpern lässt sich mikroskopisch und makroskopisch beschreiben.
Die Betrachtungsweisen sollen verknüpft werden.

\section{Dielektrische Funktion und optische Messungen}

Wirkt auf einen Isolator ein elektrisches Feld $\bm E$ so gilt für die
Polarisation $\bm P$
\begin{equation}
  \label{eq:13.1}
  \bm P = \varepsilon_0 \bm\chi \bm E
\end{equation}
mit der dielektrischen Suszeptibilität $\bm\chi$.  Diese ist im
Allgemeinen ein symmetrischer Tensor zweiter Stufe, bei kubischen
Kristallen und amorphen Festkörpern ist $\bm\chi$ ein Skalar.
\begin{equation}
  \label{eq:13.2}
  \bm\varepsilon = 1 + \bm\chi
\end{equation}
ist der Dielektrizitätstensor.  Die dielektrische Flussdichte, bzw.\
die dielektrische Verschiebung ist gegeben durch
\begin{equation}
  \label{eq:13.3}
  \bm D = \varepsilon_0 \bm E + \bm P = \varepsilon_0 \varepsilon \bm E \; .
\end{equation}
Die Größen $\bm D$ und $\bm E$ sind im Allgemeinen zeitabhängig.  Wir
machen eine Zerlegung in die spektralen Anteile mit Hilfe der
Fourier-Transformation.
\begin{equation}
  \label{eq:13.4}
  \begin{aligned}
    \bm E(t) &= \int_{-\infty}^{\infty} \bm E(\omega)\, \ee^{-\ii\omega t} \diff \omega \\
    \bm D(t) &= \int_{-\infty}^{\infty} \bm D(\omega)\, \ee^{-\ii\omega t} \diff \omega
  \end{aligned}
\end{equation}
wobei $\bm D(\omega) = \varepsilon_0 \varepsilon(\omega) \bm
E(\omega)$.

In zeitlich veränderlichen Feldern gilt die Maxwellsche Gleichung
\begin{equation}
  \label{eq:13.5}
  \rot\bm H = \bm j + \frac{\partial\bm D}{\partial t} \; .
\end{equation}
Nehmen wir zusätzlich das Ohmsche Gesetz $\bm j = \sigma \bm E$ an und
formen um, so erhalten wir
\begin{equation}
  \label{eq:13.6}
  \rot\bm H = \sigma \bm E(\omega) - \ii \omega \varepsilon_0 \varepsilon \bm E(\omega)
  = \tilde\sigma \bm E(\omega)
\end{equation}
mit $\tilde\sigma = \sigma - \ii\omega\varepsilon_0\varepsilon$ als
frequenzabhängige verallgemeinerte Leitfähigkeit.  Umgekehrt kann man auch
\begin{equation}
  \label{eq:13.7}
  \rot\bm H(\omega)
  = -\ii\varepsilon_0\tilde\varepsilon(\omega) \omega \bm E(\omega)
  = - \ii \omega \bm D(\omega)
\end{equation}
schreiben mit $\tilde\varepsilon(\omega) = \varepsilon(\omega) + \ii
\sigma / (\varepsilon_0\omega)$ als verallgemeinerte
Dielektrizitätskonstante.  Beide Beschreibungen sind austauschbar, da
sich die Unterscheidung zwischen freien und gebundenen Ladungen bei
Wechselfeldern vermischt.

Der Real- und Imaginärteil der Suszeptibilität sind über eine
\acct{Kramers-Kronig-Relation} miteinander verknüpft.  Für die
dielektrische Funktion $\varepsilon = \varepsilon' +
\ii\varepsilon''$ lautet die Kramers-Kronig-Relation
\begin{align}
  \label{eq:13.8}
  \varepsilon'(\omega) - 1 &= \frac{2}{\pi} \mathcal P
  \int_0^\infty \frac{\omega' \varepsilon''(\omega')}{\omega'^2 - \omega^2} \diff \omega' \\
  \label{eq:13.9}
  \varepsilon''(\omega) &= -\frac{2\omega}{\pi} \mathcal P
  \int_0^\infty \frac{\varepsilon'(\omega') - 1}{\omega'^2 - \omega^2} \diff \omega'
\end{align}
wobei $\mathcal P$ für den Hauptwert des Integrals steht.

Die dielektrische Funktion beschreibt die Wechselwikrung von
elektromagnetischen Wellen mit dem Festkörper.  Es ist daher nicht
verwunderlich, dass ein Zusammenhang mit den beiden optischen Größen
Brechungsindex $n'$ und Absorptionskoeffizient $\kappa$ besteht.
\begin{equation}
  \label{eq:13.10}
  \varepsilon' + \ii \varepsilon'' = (n' + \ii \kappa)^2 \; .
\end{equation}
Damit folgt sofort
\begin{equation}
  \label{eq:13.11}
  \varepsilon' = n'^2 - \kappa^2
  \quad\text{und}\quad
  \varepsilon'' = 2 n' \kappa \; .
\end{equation}

Die Reflektivität bei senkrechtem Einfall ist gegeben durch
\begin{equation}
  \label{eq:13.12}
  R = \left| \frac{\sqrt\varepsilon - 1}{\sqrt\varepsilon + 1} \right|^2
  = \frac{(n'-1)^2 + \kappa^2}{(n'+1)^2 + \kappa^2} \; .
\end{equation}

\section{Lokales Feld}

Wir wenden uns der Frage zu welches Feld an einem bestimmten Atom im
Isolator herrscht, wenn wir ein äußeres Feld an die Probe anlegen.
Legen wir ein elektrisches Feld an, so verschieben sich die Elektronen
relativ zum Atomkern und die Atome werden polarisiert.  Ihr
Dipolmoment lässt sich ausdrücken durch
\begin{equation}
  \label{eq:13.13}
  \bm p = \varepsilon_0 \alpha \bm E
\end{equation}
mit der atomaren Polarisierbarkeit $\alpha$.  Im Festkörper wirkt
nicht nur das von außen angelegte Feld $\bm E_a$, sondern auch die
Felder der benachbarten Atome.  Das Feld am Ort des Atoms bezeichnen
wir mit $\bm E_{\text{lok}}$.  Das makroskopische Feld $\bm E$, das in
die Maxwell-Gleichungen eingeht ist der Mittelwert über die lokal
variierenden Felder.  Wir wollen eine Verknüpfung zwischen dem lokalen
Feld $\bm E_{\text{lok}}$ und dem äußeren Feld $\bm E_a$ herstellen.
Vernachlässigen wir alle permanenten Dipolmomente, so zeigen alle
induzierten Dipolmoment in Feldrichtung und mikroskopisch gilt
\begin{equation*}
  \bm P = n \bm p = n \varepsilon_0 \alpha \bm E_{\text{lok}} \; .
\end{equation*}
Makroskopisch gilt hingegen nach \eqref{eq:13.1}
\begin{equation*}
  \bm P = \varepsilon_0 \chi \bm E \; .
\end{equation*}
Aus dem Vergleich der beiden Ausdrücke für die Polarisation folgt
offensichtlich
\begin{equation*}
  \chi \bm E = n \alpha \bm E_{\text{lok}} \; .
\end{equation*}
Den Zusammenhang zwischen den Feldern leitet man über folgende
Zerlegung des lokalen Feldes her
\begin{equation}
  \label{eq:13.14}
  \bm E_{\text{lok}} = \bm E_a + \bm E_D + \bm E_L + \bm E_K
\end{equation}
Um die Bedeutung der vier Beiträge zu verstehen betrachten wir eine
dünne dielektrische Platte im Plattenkondensator.  Innerhalb der
Platte denken wir uns einen kugelförmigen Bereich um das Aufatom an
dem wir die Dipol-Dipol-Wechselwirkung betrachten wollen.  Der
restliche Bereich der Platte wird mit den Mittelwerten der Felder
beschrieben.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[fill=MidnightBlue!20] (-2,-1) rectangle (2,1);
    \fill[MidnightBlue!40] (-2,-1.5) rectangle (2,-2);
    \fill[MidnightBlue!40] (-2,1.5) rectangle (2,2);
    \draw (-2,-1.5) -- (2,-1.5);
    \draw (-2,1.5) -- (2,1.5);
    \foreach \i in {-1.8,-1.4,...,1.9} {
      \node[MidnightBlue,below] at (\i,-1.5) {\tiny $+$};
      \node[MidnightBlue,below] at (\i,1) {\tiny $+$};
      \node[MidnightBlue,above] at (\i,1.5) {\tiny $-$};
      \node[MidnightBlue,above] at (\i,-1) {\tiny $-$};
    }
    \draw[fill=white] (0,0) circle (.3);
    \node[pin={[pin distance=2cm]left:Aufatom},inner sep=1pt] at (0,0) {$\times$};
    \foreach \i in {30,60,...,150} {
      \node[MidnightBlue] at (\i:.4) {\tiny $-$};
      \node[MidnightBlue] at (-\i:.4) {\tiny $+$};
    }
    \draw[DarkOrange3,->] (-1.8,-1.5) -- (-1.8,1.5);
    \draw[DarkOrange3,->] (-1.6,-1.5) -- (-1.6,-1.0);
    \draw[DarkOrange3,->] (-1.6,1.0) -- (-1.6,1.5);
    \draw[DarkOrange3,->] (-1.4,-1.5) -- (-1.4,1.5);
    \draw[DarkOrange3,->] (-1.2,-1.5) -- (-1.2,-1.0);
    \draw[DarkOrange3,->] (-1.2,1.0) -- (-1.2,1.5);
    \draw[DarkOrange3,->] (-1.0,-1.5) -- (-1.0,1.5);
    \node[pin={right:Dünne Platte}] at (1.8,0) {};
    \node[pin={[text width=4cm]right:Polarisationsladungen bzw.\ induzierte Ladungen}] at (1.8,.9) {};
    \node[pin={above right:freie Ladungen}] at (1.8,1.6) {};
  \end{tikzpicture}
  \caption{Dünne dielektrische Platte im Plattenkondensator.  Das Feld
    im Spalt zwischen Elektrode und Platte wird durch die
    Polarisationsladungen abgeschwächt.  Das Feld in der Kugel um das
    Aufatom ist größer als das in der Platte darum.}
  \label{fig:23.1}
\end{figure}

Die induzierten Ladungen auf der Oberfläche der Platte bewirken ein Feld
\begin{equation}
  \label{eq:13.15}
  \bm E_D = - \frac{1}{\varepsilon_0} \bm P.
\end{equation}
das dem äußeren Feld entgegengerichtet ist.  Im allgemeinen Fall hängt
dieses \acct{Depolarisationsfeld} von der Geometrie der Probe ab, d.h.
\begin{equation}
  \label{eq:13.16}
  \bm E_D = - f \frac{1}{\varepsilon_0} \bm P
\end{equation}
mit dem Depolarisationsfaktor $f$.  Wie man schon aus \eqref{eq:13.15}
sieht ist der Depolarisationsfaktor für eine dünne Platte $f=1$.  Eine
Kugel hat den Depolarisationsfaktor $f=1/3$ und ein langer dünner
Zylinder $f=0$.  Um zu geometrieunabhängigen Aussagen zu kommen fasst
man das äußere Feld $\bm E_a$ und das Depolarisationsfeld $\bm E_D$
zum makroskopischen Feld $\bm E = \bm E_a + \bm E_D$ zusammen.

Nun kommen wir zu dem kugelförmigen Bereich um das Aufatom herum.
Betrachte dazu eine geladene Kugel mit Radius $R$.  Das
\acct{Lorentz-Feld} $E_L$ wird durch die Polarisation der
Kugeloberfläche hervorgerufen.
\begin{equation}
  \label{eq:13.17}
  E_L = \frac{P}{3 \varepsilon_0} \; .
\end{equation}

Innerhalb der Kugel um das Aufatom liegen weitere Nachbaratome, die
ebenfalls einen Einfluss auf das Feld am Aufatom haben.  Dieser
Einfluss der Nachbaratome hängt von der Kristallstruktur ab.  Der
Beitrag der Nachbaratome kompensiert sich weitestgehend und das
resultierende \acct{Kristall-Feld} $E_K$ ist klein.  Für eine kubische
Anordnung der Nachbaratome verschwindet $E_K$ sogar.

Das Endergebnis für das lokale Feld in kubischer Anordnung ist die
Lorentz-Beziehung
\begin{equation}
  \label{eq:13.18}
  \boxed{
    \bm E_{\text{lok}} = \bm E + \frac{\bm P}{3 \varepsilon_0}
  } \; .
\end{equation}

Aus den Beziehungen $\chi \bm E = n \alpha \bm E_{\text{lok}}$ und
$\bm P = \varepsilon_0 \chi \bm E$ folgt mit \eqref{eq:13.18}
\begin{equation}
  \label{eq:13.19}
  \chi = \frac{n\alpha}{1-\frac{n\alpha}{3}} \; .
\end{equation}
Ohne Berücksichtigung des lokalen Feldes wäre $\chi = n\alpha$, und mit
$\varepsilon = 1+ \chi$ folgt die Clausius-Mosotti-Beziehung
\begin{equation}
  \label{eq:13.20}
  \boxed{
    \frac{\varepsilon-1}{\varepsilon+2} = \frac{n\alpha}{3}
  } \; .
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: