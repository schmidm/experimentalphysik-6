Man unterscheidet zwei Exzitonen-Grundtypen.  Zum einen die
\acct{Frenkel-Exzitonen}, welche in Alkalihalogeniden wie \ch{NaCl}
oder \ch{KCl}, Molekülkristallen oder Edelgaskristallen auftreten.
Zum anderen die \acct{Mott-Wannier-Exzitonen}, die in Halbleitern wie
\ch{GaAs}, \ch{Si} oder \ch{Ge} angeregt werden können.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,eh/.style={fill=white,circle,inner sep=0pt}]
    \begin{scope}
      \node at (0,2.5) {Frenkel};
      \draw (-2,-2) rectangle (2,2);
      \foreach \x in {-1.5,-1,...,1.5} {
        \foreach \y in {-1.5,-1,...,1.5} {
          \node[dot] at (\x,\y) {};
        }
      }
      \draw[decorate,decoration=brace] (1.6,0) -- node[right] {$a$} (1.6,-.5);
      \draw[MidnightBlue] (-.5,0) ellipse (.2 and .3);
      \node[MidnightBlue,eh] at (-.5,.3) {$+$};
      \node[MidnightBlue,eh] at (-.5,-.3) {$-$};
      \node[below,text width=4.5cm] at (0,-2) {
        \begin{itemize}
        \item $r\approx a$
        \item $e$, $h$ am selben Ort
        \item $e$-$h$-Paar stark gebunden
        \item $E_B \approx \SI{1}{\eV}$
        \end{itemize}
      };
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \node at (0,2.5) {Mott-Wannier};
      \draw (-2,-2) rectangle (2,2);
      \foreach \x in {-1.5,-1,...,1.5} {
        \foreach \y in {-1.5,-1,...,1.5} {
          \node[dot] at (\x,\y) {};
        }
      }
      \draw[decorate,decoration=brace] (1.6,1.5) -- node[right] {$a$} (1.6,1);
      \draw[MidnightBlue] (0,0) circle (1.6);
      \node[MidnightBlue,eh] (plus) at (50:1.6) {$+$};
      \node[MidnightBlue,eh] at (50:-1.6) {$-$};
      \draw[MidnightBlue,->] (0,0) -- node[below right] {$r$} (plus);
      \node[below,text width=4.5cm] at (0,-2) {
        \begin{itemize}
        \item $r \gg a$
        \item $E_B \propto \SI{1}{\milli\eV}\ldots\SI{50}{\milli\eV}$
        \end{itemize}
      };
    \end{scope}
  \end{tikzpicture}
  \caption{Vergleich von Frenkel- und Mott-Wannier-Exzitonen.}
  \label{fig:27.1}
\end{figure}

\paragraph{Frenkel-Exzitonen}
Auf Grund der Coulomb- und Austausch-Wechselwirkung $J$ findet eine
Energieübertrag zwischen den Ionen (z.B.\ \ch{Cl^-} bei \ch{NaCl})
statt.  Dies ermöglicht den Transport von Anregungsenergie durch den
Festkörper.  Frenkel-Exzitonen besitzen in der Regel starke
Wechselwirkung mit Licht.

\paragraph{Mott-Wannier-Exzitonen}
Aus der Schrödingergleichung lässt sich für parabolische,
nicht-entartete, isotrope Leitungs- und Valenzbänder eine
effektive-Masse-Gleichung für die Wellenfunktion der
Mott-Wannier-Exzitonen ableiten
\begin{equation}
  \label{eq:13.58}
  \left( - \frac{\hbar^2}{2 m_e^*} \nabla_e^2 - \frac{\hbar^2}{2 m_h^*} \nabla_h^2
    - \frac{e^2}{4 \pi \varepsilon_0 \varepsilon} \frac{1}{|r_e - r_h|} \right)
  \psi(\bm r_e,\bm r_h) = (E_{\text{Ges}} - E_g) \psi(\bm r_e,\bm r_h) \; .
\end{equation}
Die Einführung von Relativ- und Schwerpunktskoordinaten führt zu einer
Faktorisierung in zwei Gleichungen.  Die Energieeigenwerte spalten
ebenfalls in zwei Anteile auf
\begin{equation}
  \label{eq:13.59}
  E_{\text{Ges}} = E_g
  + \overbrace{\frac{\hbar^2 K^2}{2 M}}^{\mathclap{\text{Schwerpunktsbewegung}}}
  - \underbrace{\frac{1}{\varepsilon^2} \left( \frac{m_{\text{red}}}{m_0} \right)
      \frac{R_y}{n^2}}_{\text{Relativbewegung}}
\end{equation}
mit $K = k_e + k_h$ und $M = m_e + m_h$. Die reduzierte Masse $m_{\text{red}}$ ist gegeben durch
\begin{equation*}
  \frac{1}{m_{\text{red}}} = \frac{1}{m_e} + \frac{1}{m_h} \; .
\end{equation*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,width=6cm,
      xlabel={Wellenzahl $k$},ylabel={Energie $E$},
      xmin=-1.5,xmax=1.5,ymin=-2,ymax=.5,
      xtick=\empty,ytick=\empty,smooth]
      \pgfplotsinvokeforeach{1,2,3}{
        \addplot[MidnightBlue!20,domain=-2:-sqrt(1/#1)] {x^2-1/#1};
        \addplot[MidnightBlue,domain=-sqrt(1/#1):sqrt(1/#1)] {x^2-1/#1};
        \addplot[MidnightBlue!20,domain=sqrt(1/#1):2] {x^2-1/#1};
      }
      \draw (axis cs:-2,0) -- (axis cs:2,0);
      \draw (axis cs:0,-2) -- (axis cs:0,.5);
      \draw[DarkOrange3,<->] (axis cs:0,-2) -- node[right] {$h\nu$} (axis cs:0,-1);
      \draw[dashed] (axis cs:-1,-1) -- (axis cs:0,-1);
      \draw[<->] (axis cs:-.9,-1) -- node[left] {$E_x$} (axis cs:-.9,0);
    \end{axis}
  \end{tikzpicture}
  \caption{Energieschema der Exzitonen.  Im Grundzustand des Kristalls
    (unterer Rand des Graphen) sind keine Exzitonen vorhanden.  Wurde
    ein Exziton angeregt, so zerlegt sich dessen Bewegung in Relativ-
    und Schwerpunktsbewegung.}
  \label{fig:27.2}
\end{figure}

\section{Optische Eigenschaften freier Ladungsträger}

In diesem Abschnitt beschäftigen wir uns mit Wechselwirkungen von
elektromagnetischen Wellen mit den freien Ladungsträgern in Metallen
und stark dotierten Halbleitern.  In teilbesetzten Bändern sind
Intrabandübergänge möglich.

Wir wollen die dielektrische Funktion herleiten.  Dazu betrachten wir
die eindimensionale Bewegung eines Elektrons im periodischen Feld.
Die Differentialgleichung dafür lautet
\begin{equation}
  \label{eq:13.60}
  m^* \ddot{u} + \frac{m^* \dot{u}}{\tau} = -e E(t) \; .
\end{equation}
Es existiert keine rücktreibende Kraft, daher ist $\omega_0 = 0$.
Weiterhin ist $\tau$ die Stoßzeit und $\sigma = n e^2 \tau/m^*$ die
Leitfähigkeit.  Das periodische Feld folgt der Dynamik $E(t) = E_0
\ee^{-\ii(\omega t - k x)}$.  Die Rechnung verläuft wie bei
\eqref{eq:13.22} und für die dielektrische Funktion folgt
\begin{equation}
  \label{eq:13.61}
  \varepsilon(\omega) = 1
  + \underbrace{\chi_{\text{el}}}_{\text{geb.\ $e^-$}}
  - \underbrace{\frac{ne^2}{\varepsilon_0 m^*}
    \frac{1}{\omega^2 + \ii\omega/\tau}}_{\text{freie $e^-$ aus \eqref{eq:13.60}}} \; .
\end{equation}
Die mittlere Stoßzeit der Elektronen in Metallen mittlerer Reinheit
liegt bei Zimmertemperatur in der Größenordnung von \SI{e-14}{\s}.
Die Dämpfung kann bei optischen Frequenzen vernachlässigt werden (für
eine Rechnung mit Dämpfung siehe \textcite{gross}).  Für optische
Frequenzen können wir \eqref{eq:13.61} vereinfachen
\begin{equation}
  \label{eq:13.62}
  \varepsilon(\omega)
  = \underbrace{\varepsilon_{\infty}}_{\mathclap{\text{Beitrag der geb.\ $e^-$}}}
  - \frac{ne^2}{\varepsilon_0 m^*} \frac{1}{\omega^2}
  = \varepsilon_{\infty}\left(1 - \frac{\omega_p^2}{\omega^2}\right)
\end{equation}
mit der \acct{Plasmafrequenz}
\begin{equation}
  \label{eq:13.63}
  \boxed{
    \omega_p^2 = \frac{n e^2}{\varepsilon_0 \varepsilon_\infty m^*}
  } \; .
\end{equation}

\subsection{Ausbreitung elektromagnetischer Wellen in Metallen}

Für elektromagnetische Wellen gilt die Wellengleichung
\eqref{eq:13.45}.  Setzen wir eine ebene Welle der Form $E(t) = E_0
\ee^{-\ii(\omega t-kx)}$ an, erhalten wir die Dispersionsbeziehung
\begin{equation}
  \label{eq:13.64}
  \varepsilon(\omega) \omega^2 = c^2 k^2 \; .
\end{equation}
Setzen wir nun die in \eqref{eq:13.62} gefundene dielektrische
Funktion für optische Frequenzen in \eqref{eq:13.64} ein, so folgt
\begin{equation}
  \label{eq:13.65}
  \varepsilon_\infty \omega^2 \left(1 - \frac{\omega_p^2}{\omega^2}\right) = c^2 k^2 \; .
\end{equation}
In dieser Funktion treten zwei Bereiche mit unterschiedlichen
Eigenschaften auf.  Im Bereich $\omega < \omega_p$ ist offensichtlich
$k^2 < 0$, also die Wellenzahl komplex.  Es ist keine Ausbreitung von
Wellen möglich.  Für $\varepsilon(\omega) < 0$ findet also
Totalreflexion statt.  Im Bereich $\omega > \omega_p$ ist hingegen
immer $\varepsilon(\omega) > 0$.  Wenn wir \eqref{eq:13.65} dann nach
$\omega$ umstellen finden wir
\begin{equation}
  \label{eq:13.66}
  \omega^2 = \omega_p^2 + \frac{c^2 k^2}{\varepsilon_\infty} \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,width=6cm,
      domain=0:3,legend pos=outer north east,
      xlabel={Wellenzahl $ck/\omega_p$},ylabel={Frequenz $\omega/\omega_p$}]
      \fill[MidnightBlue!20] (axis cs:0,0) rectangle (axis cs:3,1);
      \draw (axis cs:0,1) -- (axis cs:3,1);
      \addplot[MidnightBlue] {sqrt(1+x^2)};
      \addplot[DarkOrange3] {x};
      \legend{$\sqrt{\smash[b]{\omega_p^2}+c^2 k^2}$,$c k$};
    \end{axis}
  \end{tikzpicture}
  \caption{Dispersionsrelation von elektromagnetischen Wellen in
    Metallen.  Der verbotene Bereich, in dem keine Propagation von
    Wellen möglich ist, ist hellblau hinterlegt.}
  \label{fig:27.3}
\end{figure}

\subsection{Longitudinale Schwingungen des Elektronengases: Plasmonen}

Im Gas freier Elektronen sind longitudinale Schwingungen möglich, die
allerdings nicht an elektromagnetische Wellen ankoppeln.  Das liegt
daran, dass die Felder senkrecht aufeinander stehen.  Ein
Durchstrahlen von Metallfilmen mit Elektronen führt zur Anregung von
Plasmaschwingungen.  Im Grenzfall $k\to 0$ schwingen alle Elektronen
und Ionen gegeneinander und es baut sich eine Auslenkung $u$ wie in
Abbildung~\ref{fig:27.4} auf.  An den Rändern der Probe baut sich
durch den Überhang der durch die Auslenkung entsteht eine
Flächenladung auf.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \fill[fill=MidnightBlue!20] (-2,-1.5) rectangle (2,-1);
    \fill[fill=MidnightBlue!40] (-2,1) rectangle (2,1.5);
    \draw (-2,-1.5) rectangle (2,1.5);
    \draw (-2,1) -- (2,1);
    \foreach \i in {-1.5,-1,...,1.5} {
      \node[MidnightBlue] at (\i,1.25) {$-$};
      \node[MidnightBlue] at (\i,-1.25) {$+$};
    }
    \draw[MidnightBlue,->] (0,-.5) -- node[right] {$E$} (0,.5);
    \node[right] at (2,1.25) {nur Elektronen};
    \node[right] at (2,-1.25) {nur Ionen};
    \draw (-2,-1) -- (2,-1);
    \draw[<->] (-2.2,-1.5) -- node[left] {$u$} (-2.2,-1);
  \end{tikzpicture}
  \caption{Im Grenzfall $k\to0$ schwingen alle Elektronen und Ionen
    gegeneinander.  Die Auslenkung $u$ der beiden gegeneinander
    bewirkt eine Flächenladung des Überhangs und damit ein
    elektrisches Feld.}
  \label{fig:27.4}
\end{figure}

Die Flächenladung verursacht ein elektrisches Feld
\begin{equation}
  \label{eq:13.67}
  E = \frac{neu}{\varepsilon_0 \varepsilon_\infty}
\end{equation}
$\varepsilon_\infty$ berücksichtigt auch die Polarisierbarkeit der
Rumpf-Elektronen.  Die Bewegungsgleichung der freien Elektronen (ohne
Dämpfung) lautet
\begin{equation}
  \label{eq:13.68}
  nm\ddot u(t) = -neE(t) = - \frac{n^2 e^2 u(t)}{\varepsilon_0\varepsilon_\infty}
\end{equation}
oder
\begin{equation*}
  \ddot u + \omega_p^2 u = 0 \; .
\end{equation*}
Dies entspricht einem harmonischer Oszillator mit der Plasmafrequenz
$\omega_p$.  Die gleiche Schwingungsfrequenz erhält man auch aus
\eqref{eq:13.62} mit $\varepsilon(\omega_\ell = \omega_p) = 0$.  Wie
bei den Phonon-Polaritonen stellt auch hier die Frequenz der
longitudinalen Schwingung eine untere Grenze für die Ausbreitung von
elektromagnetischen Wellen dar.

Die Plasmaschwingungen sind kohärente, kollektive Anregungen aller
Elektronen des Fermigases.  Die Anregungen sind quantisiert mit
$\hbar\omega_p$ und werden Plasmonen genannt.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: