\subsection{Chemische Verschiebung}

Misst man den spektralen Bereich eines Isotops (z.B.\ \ch{^1H},
\ch{^{13}C}) mit hoher Auflösung, so findet man meist nicht nur eine
Resonanzlinie, sondern praktisch so viele wie unterscheidbare Kerne in
der Probe vorhanden sind. Dies nutzt man zur Strukturaufklärung von
Molekülen.

\begin{example}
  Ethanol \ch{CH3CH2OH}, hat drei Gruppen von Protonen, nämlich
  \ch{CH3}, \ch{CH2} und \ch{OH}, also drei Gruppen von Linien.  Die
  relativen Intensitäten sind $3:2:1$, was offensichtlich von der
  Anzahl der Protonen abhängt.
\end{example}

Für die Resonanz nach Gleichung \eqref{eq:12.6} ist nicht das
angelegte Feld, sondern das lokale Feld des Kerns maßgeblich.  Durch
Anlegen von $B_0$ wird im Molekül ein Strom induziert und damit ein
magnetisches Moment der Elektronen erzeugt, das dem äußeren Feld nach
der Lenzschen Regel entgegenwirkt.  Man spricht von der
diamagnetischen Abschirmung.  Das effektive Feld ist also um das
induzierte Feld abgeschwächt.  Dieses ist proportional zum angelegten
Feld und es gilt
\begin{equation}
  \label{eq:12.8}
  B_{\text{eff}} = B_0 - B_{\text{ind}} = B_0 - \sigma B_0 = B_0 (1-\sigma)
\end{equation}
mit der Abschirmkonstanten $\sigma$.  Der Term $\sigma B_0$ heißt
\acct[Chemische Verschiebung]{chemische Verschiebung}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[->] (0,-1.5) -- (0,1.5) node[right] {$B_0$};
    \draw (0,0) circle (1);
    \draw[DarkOrange3,->] (0,.5) ++(80:.5 and .1) arc (80:-260:.5 and .1);
    \draw[DarkOrange3,->] (0,-.5) ++(80:.5 and .1) arc (80:-260:.5 and .1);
    \draw[MidnightBlue,<-] (135:1) to[out angle=135,in angle=-135,curve through={(-1.5,0)}] (-135:1);
    \draw[MidnightBlue,<-] (45:1) to[out angle=45,in angle=-45,curve through={(1.5,0)}] (-45:1);
    (1.1,0) .. controls (1.1,-1) and (.5,-1.5) .. (0,-1);
  \end{tikzpicture}
  \caption{Die diamagnetische Abschirmung bewirkt die chemische
    Verschiebung.}
  \label{fig:20.1}
\end{figure}

Die Abschirmkonstante $\sigma$ hängt von der Dichte und dem
Bindungszustand der Elektronen in der Nähe des Kerns ab.  Im
allgemeinen Fall ist $\sigma$ ein Tensor $\bm\sigma$.  Dies ist
insbesondere im Festkörper zu beachten.  Durch die Abschirmung
verändert sich natürlich die Resonanzbedingung \eqref{eq:12.6} und es
gilt
\begin{equation}
  \label{eq:12.9}
  h \nu = g_I \mu_k B_{\text{lok}} = g_I \mu_k B_0 (1-\sigma) \; .
\end{equation}
Das induzierte Feld kann auch am Ort des betrachteten Kerns
gleichgerichtet sein, wie es z.B.\ bei Benzol der Fall ist.  Die
chemische Verschiebung wird gegenüber der Resonanzfrequenz~$\nu_0$
eines Standards in ppm\footnote{parts per million} gemessen.  Für
$B_{\text{Standard}}$ benutzt man Tetramethylsilen \ch{Si(CH3)4} (TMS)
welches zwölf äquivalente Protonen hat.  Es gilt
\begin{equation*}
  \delta = \frac{B_{\text{Standard}} - B_{\text{Probe}}}{B_{\text{Standard}}}
  \cdot \num{e6} \quad [\si{ppm}] \; .
\end{equation*}
Die chemische Verschiebung muss nicht zwingend gegen den Nullpunkt
gemessen werden, da dies messtechnisch schwer möglich ist.  Misst man
die chemische Verschiebung in einer unbekannten Probe, so kann man aus
ihr Rückschlüsse auf die vorhandenen Molekülbindungen und Gruppen
ziehen.

\subsection{Magnetische Dipol-Dipol-Wechselwirkung}

Ein magnetischer Dipol $\mu_1$ im Ursprung des Koordinatensystems
erzeugt ein Feld.  Aus der Elektrodynamik ist bekannt, dass dieses Feld durch
\begin{equation}
  \label{eq:12.10}
  \bm B_{\mu_1}(\bm r) = \frac{\mu_0}{4\pi r^5} [3 (\bm \mu \cdot \bm r)\bm r
  - (\bm r \cdot \bm r) \bm \mu]
\end{equation}
gegeben ist.  Ein zweiter Dipol $\mu_2$ am Ort $\bm r= \bm r_{12}$
spürt dieses Magnetfeld.  Die Wechselwirkungsenergie berechnet sich
mit $W_{12} = - \bm \mu_2 \cdot \bm B_{\mu_1}$ zu%
\refstepcounter{equation}%
\refstepcounter{equation}%
\begin{equation}
  \label{eq:12.13}
  W = \frac{\mu_0}{4\pi} \left( \frac{\bm\mu_1 \bm\mu_2}{r_{12}^3}
    - 3 \frac{(\bm\mu_1 \bm r_{12}) (\bm\mu_2 \bm r_{12})}{r_{12}^5} \right) \; .
\end{equation}
Siehe auch Abbildung~\ref{fig:20.2} zur besseren Veranschaulichung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[dashed] (0,-2) -- (0,2);
    \foreach \i in {.4,.6,.8} {
      \draw[MidnightBlue,dotted] (\i,0) ellipse ({\i} and {2*\i});
      \draw[MidnightBlue,dotted] (-\i,0) ellipse ({\i} and {2*\i});
    }
    \draw[MidnightBlue,->] (0,-.3) -- node[left] {$\bm\mu_1$} (0,.3);
    \draw[MidnightBlue,->] (-2,-1) -- node[left] {$\bm B_0$} (-2,1);
    \draw (0,0) -- node[below,pos=.8] {$\bm r_{12}$} (20:3);
    \draw[MidnightBlue,->] (20:3) +(0,-.3) -- node[right] {$\bm\mu_2$} +(0,.3);
    \draw[DarkOrange3] (0,.3) arc(90:20:.3);
    \node[DarkOrange3] at (55:.5) {$\theta$};
  \end{tikzpicture}
  \caption{Skizze zur Dipol-Dipol-Wechselwirkung.  Ein Dipol
    $\bm\mu_1$ im Ursprung erzeugt ein Feld, in dessen Einfluss sich
    $\bm\mu_2$ bewegt.}
  \label{fig:20.2}
\end{figure}

Diese Wechselwirkungsenergie muss bei der ESR und NMR berücksichtigt
werden.  Für die magnetischen Dipolmomente $\mu_1$ und $\mu_2$ lassen
sich alle Kombinationen der Momente von Kern und Spin
\begin{align*}
  \mu_I &= g_I \mu_k \bm I \;,\quad\text{(Kern)} \\
  \mu_S &= g_e \mu_B \bm S \;,\quad\text{(Spin)}
\end{align*}
verwenden.  Die verschiedenen Kombinationen tragen verschiedene Namen,
die in Tabelle~\ref{tab:20.1} aufgelistet sind.

\begin{table}[tb]
  \centering
  \begin{tabular}{ccc}
    \toprule
    $\mu_1$ & $\mu_2$ & Effekt \\
    \midrule
    $S$ & $S$ & ESR-Feinstruktur \\
    $S$ & $I$ & ESR-Hyperfeinstruktur \\
    $I$ & $I$ & NMR-Feinstruktur \\
    \bottomrule
  \end{tabular}
  \caption{Namen der verschiedenen Dipol-Dipol-Wechselwirkungen.}
  \label{tab:20.1}
\end{table}

Nehmen wir an, beide Dipole wären durch ein starkes Magnetfeld entlang
der $z$-Achse ausgerichtet.  Es bleiben in \eqref{eq:12.13} nur die
$z$-Komponenten der Terme übrig.  Es gilt
\begin{equation*}
  \frac{\bm\mu_1 \bm r_{12}}{|\bm r_{12}|} = \mu_1 \cos \theta \; .
\end{equation*}
Der zweite Term wird damit zu
\begin{equation*}
  \frac{3}{\bm r_{12}^2} (\bm\mu_1 \bm r_{12}) (\bm\mu_2 \bm r_{12})
  = \frac{3}{\bm r_{12}^2} \mu_1 \cos^2\theta r_{12}^2 \mu_2
  = 3 \cos^2\theta \mu_1 \mu_2 \; .
\end{equation*}
Damit bleibt für die Wechselwirkungsenergie%
\begin{equation}
  \label{eq:12.14}
  W = \frac{\mu_0}{4\pi} \frac{1}{r_{12}^3} \mu_1 \mu_2 [1-3\cos^2\theta] \; .
\end{equation}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,domain=0:90,width=6cm,
      xlabel={Winkel $\theta$},ylabel={$1-3\cos^2\theta$},xtick=\empty,
      extra x ticks={0,54.7356,90},extra x tick labels={$0$,$\theta_m$,$\pi/2$}]
      \draw (axis cs:0,0) -- (axis cs:90,0);
      \draw[dashed] (axis cs:54.7356,-2) -- (axis cs:54.7356,1);
      \addplot[MidnightBlue] {1-3*cos(x)^2};
    \end{axis}
  \end{tikzpicture}
  \caption{Bei $\theta_m \approx \SI{54}{\degree}$ liegt der
    \enquote{magische Winkel}, bei dem die Dipol-Dipol-Wechselwirkung
    gerade Null wird.}
  \label{fig:20.3}
\end{figure}

Betrachten wir nun die direkte Kernspin-Kernspin-Kopplung.  Sei der
Kern $A$ im Zustand $m_I$ und der Kern $B$ befinde sich im Abstand
$\bm r_{12}$ zu Kern $A$.  Das Feld, das der Kern $A$ am Ort des Kerns
$B$ erzeugt hängt von der Richtung $\theta$ der Kernverbindungslinie
zur Feldrichtung $\bm B_0$ ab.  Die $z$-Komponente hat den Wert
\begin{equation}
  \label{eq:12.15}
  B_A = - \frac{\mu_0}{4\pi} \underbrace{g_I \mu_k m_I}_{\mu_I}
  \frac{1}{r_{12}^3} (1-3\cos^2\theta) \; .
\end{equation}
Wir nehmen an, der Kern $A$ sei ein Proton mit $I=1/2$.  Es gibt also zwei
Einstellmöglichkeiten $m_I = \pm 1/2$.  Der Kern $B$ sieht ein
effektives Feld $B_0 \pm B_A$.  Für den Kern $B$ existieren zwei
Resonanzfelder mit Abstand $2 B_A$, das Resonanzsignal wird ein
Dublett.  Den Energieabstand der beiden Linien des Dubletts nennt man
$J$, die Spin-Spin-Kopplungskonstante.  Die gleiche Überlegung gilt
umgekehrt auch für Kern $B$.

Für ein HD-Molekül gilt zum Beispiel
\begin{align*}
  I_D = 1 \;&,\quad m_I = 1,0,-1 \\
  I_H = 1/2 \;&,\quad m_I = -1/2,1/2
\end{align*}
Die Anzahl der Linien entspricht der Multiplizität $2I+1$, siehe dazu
Abbildung~\ref{fig:20.4}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[axis on top,enlargelimits=false,
      domain=0:4,ymin=-.2,ymax=1.2,samples=100,width=4cm,
      xlabel={Magnetfeld $B$},ylabel={\ch{D}-Signal},xtick=\empty,ytick=\empty]
      \addplot[smooth,MidnightBlue] {exp(-50*(x-1)^2)+exp(-50*(x-2)^2)};
    \end{axis}
    \begin{axis}[shift={(5cm,0)},axis on top,enlargelimits=false,
      domain=0:4,ymin=-.2,ymax=1.2,samples=100,width=4cm,
      xlabel={Magnetfeld $B$},ylabel={\ch{H}-Signal},xtick=\empty,ytick=\empty]
      \addplot[smooth,MidnightBlue] {exp(-50*(x-1)^2)+exp(-50*(x-2)^2)+exp(-50*(x-3)^2)};
    \end{axis}
  \end{tikzpicture}
  \caption{Die Anzahl der Linien, die im Signal sichtbar sind
    entspricht der Multiplizität des jeweiligen Kerns.}
  \label{fig:20.4}
\end{figure}

Für einen Kernabstand von $r_{12} = \SI{0.2}{\mm}$ erhält man für
$B_A$ typische Werte von $B_A \sim \SI{e-4}{\tesla}$.

In flüssiger oder gasförmiger Phase ändert sich jedoch normalerweise
der Winkel $\theta$ zwischen den wechselwirkenden Kernen relativ zu
$B_0$ rasch im Vergleich zur Kernspin"=Resonanzfrequenz.  Der Term
$(1-3\cos^2\theta)$ wird daher im Zeitmittel Null, das Magnetfeld wird
ebenfalls $B_{\text{Kern}} = 0$.  Die Dipol-Dipol-Wechselwirkung wird
ausgemittelt, man nennt dies Bewegungsverschmälerung.  Große
biologische Moleküle bewegen sich langsamer und es findet eventuell
keine vollständige Ausmittelung statt.

\subsection{Indirekte Kernspin-Kernspin-Kopplung}

Die indirekte Kernspin-Kernspin-Kopplung wird durch die
Bindungselektronen zwischen den Kernspins vermittelt.  Sie wird nicht
durch die Bewegung ausgemittelt und ist unabhängig von $B_0$.  Die
Grundlage dafür ist die Fermi-Kontakt-Wechselwirkung zwischen den
magnetischen Momenten von Kern und Elektronen.  Die indirekte
Kernspin-Kernspin-Kopplung ist um den Faktor \num{e2} bis \num{e4}
kleiner als die direkte Kernspin-Kernspin-Wechselwirkung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}
      \node[draw,circle,dotted,inner sep=1pt] (A) at (-.7,0) {$\uparrow\Downarrow$};
      \node at (-.7,.8) {$I_A S_A$};
      \node[circle,inner sep=1pt] (B) at (.7,0) {$\Uparrow\downarrow$};
      \node at (.7,.8) {$S_B I_B$};
      \draw (-.7,.5) arc(90:270:.5) to[out=0,in=180] (0,-.3) to[out=0,in=180] (.7,-.5)
      arc(270:450:.5) to[out=180,in=0] (0,.3) to[out=180,in=0] (-.7,.5);
      \draw[DarkOrange3,<->] (A) to[out=120,in=60,looseness=1.5]
      node[above=.1cm] {Indirekte Kernspin-Kernspin WW} (B);
      \draw[DarkOrange3,<->] (A) to[out=-60,in=-120] node[below] {Pauli-Prinzip} (B);
    \end{scope}
    \begin{scope}[shift={(4,0)}]
      \node at (-.7,0) {$\uparrow\Downarrow$};
      \node at (-.7,.8) {$I_A S_A$};
      \node at (-.7,-.8) {$^{13}$C};
      \node at (.7,0) {$\Uparrow\uparrow$};
      \node at (.7,.8) {$S_B I_B$};
      \node at (.7,-.8) {$^1$H};
      \draw (-.7,.5) arc(90:270:.5) to[out=0,in=180] (0,-.3) to[out=0,in=180] (.7,-.5)
      arc(270:450:.5) to[out=180,in=0] (0,.3) to[out=180,in=0] (-.7,.5);
    \end{scope}
    \begin{scope}[shift={(.5,-2.5)}]
      \draw (0,0) node[left] {ohne Kopplung} -- (3,0);
      \draw[MidnightBlue] (1,0) -- (1,1) node[above] {C};
      \draw[MidnightBlue] (2,0) -- (2,1) node[above] {H};
      \draw[<->] (1,.7) -- node[above] {$\Delta\delta$} (2,.7);
      \draw (0,-1.5) node[left] {mit Kopplung} -- (3,-1.5);
      \draw[MidnightBlue,dashed] (1,-1.5) -- (1,-.5);
      \draw[DarkOrange3] (1-.3,-1.5) -- (1-.3,-.5);
      \draw[DarkOrange3] (1+.3,-1.5) -- (1+.3,-.5);
      \draw[MidnightBlue,dashed] (2,-1.5) -- (2,-.5);
      \draw[DarkOrange3] (2-.3,-1.5) -- (2-.3,-.5);
      \draw[DarkOrange3] (2+.3,-1.5) -- (2+.3,-.5);
      \draw[<->] (1-.3,-1.7) -- node[below] {$J$} (1+.3,-1.7);
      \draw[<->] (2-.3,-1.7) -- node[below] {$J$} (2+.3,-1.7);
    \end{scope}
  \end{tikzpicture}
  \caption{Zwei Kerne $A$ und $B$ haben den Kernspin $I_A$ und $I_B$
    und den Elektronenspin $S_A$ und $S_B$ der Bindungselektronen.
    Der Kernspin $I_A$ polarisiert den Elektronenspin $S_A$.  Nach dem
    Pauli-Prinzip muss dann $S_B$ dem entgegengesetzt stehen.  Damit
    ist eine antiparallele Ausrichtung von $I_B$ zu $I_A$ energetisch
    günstiger (oben links).  Die Einstellung zweier paralleler
    Kernspins ist weniger günstig (oben rechts) weil dann z.B.\ $I_B$
    und $S_B$ parallel sind.  Da zwei Einstellungen möglich sind
    spalten die ehemals separaten Linien in zwei Linien auf (unten).}
  \label{fig:20.5}
\end{figure}

In Abbildung~\ref{fig:20.5} ist die indirekte
Kernspin-Kernspin-Wechselwirkung der \ch{^{13}C}-\ch{^1H}-Bindung zu
sehen.  Beide Kerne haben den Spin $I_A = I_B = 1/2$.  Der Spin $S_B$
des Bindungselektrons von \ch{^1H} steht antiparallel zum Spin $S_A$
des Bindungselektrons von \ch{^{13}C} auf Grund des Pauli-Prinzips.
Es gibt zwei Ausrichtungsmöglichkeiten für $I_B$, wobei $\Uparrow\dn$
energetisch günstiger als $\Uparrow\up$ ist, wie es aus der
Fermi-Kontakt-Wechselwirkung folgt.  Der Energieabstand zwischen den
beiden Einstellungen wird wieder mit der Spin-Spin-Kopplungskonstante
$J$ in \si{\per\s} gemessen.  Ohne Kopplung gibt es für die separaten
\ch{^{13}C}- und \ch{^1H}-Kerne jeweils eine NMR-Resonanzlinie.  Diese
Linie spaltet jedoch durch die von den Bindungselektronen vermittelte
indirekte Kernspin-Kernspin-Kopplung in zwei Linien mit Abstand $J$
auf.  Es gilt zu beachten, dass die beiden Kernspins durch die
indirekte Wechselwirkung nicht in einer antiparallelen Ausrichtung
fixiert werden.  Die beiden Konfigurationen, die parallele und
antiparallele Ausrichtung, treten gleich häufig auf, unterscheiden
sich aber in ihrer Energie.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../ExPhys6.tex"
%%% End: